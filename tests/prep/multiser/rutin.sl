(fluid '(multioutpb multiouth oldoutch oldinch))

% Create and return an index list of length n, with first
% item T and the other items NIL.

(de initindex (n)
 (prog	(rt)	% Return list
	(cond ((not (and (numberp n) (lessp 0 n)))
		(error 0 "Bad input to initindex")))
	(setq rt (ncons T))
  ml	(cond ((lessp 0 (setq n (sub1 n)))
		(setq rt (cons NIL rt))
		(go ml)))
	(return (reverse rt)) ))


% Update the input ind, an index list. Changes it input.
% Returns the changed index, if there was one. Else, returns NIL.

(de updateindex (ind)
 (prog	(tl)	% Tail of ind (the index list)
	(setq tl ind)
  ml	(cond ((car tl)
		(rplaca tl NIL))
	      (T
		(rplaca tl T)
		(return ind)))
	(cond ((setq tl (cdr tl))
		(go ml))) ))

% This generates the matrix product of the list of 0:s and 1:s
% with the given column lforms of Lispforms of terms

(de index2pol (ind lforms)
 (prog	(ip lp rt)	% ind position, lforms position, return
	(setq ip ind)
	(setq lp lforms)
  ml	(cond ((car ip)
		(setq rt (append (car lp) rt))))
	(cond ((setq ip (cdr ip))
		(setq lp (cdr lp))
		(go ml)))
	(return rt) ))

% This is degreeenddisplay adopted for this case.

(de multided NIL
 (progn	(setq oldoutch (wrs multioutpb))
	(tdegreecalculatepbseries (getcurrentdegree))
	(channelflush multioutpb)

	(wrs multiouth)
	(tdegreehseriesout (getcurrentdegree))
	(channelflush (wrs oldoutch)) ))


% The main procedure. The two first inputs should be existing files.
% The first one should contain mode settings (including maxdeg
% and embdim), and should set multihilbertin to a lisp form of
% all basic input relations (which are not to be changed).
% The second one should contain the terms of which the last relation
% is to be composed, as a lisp list of lisp forms.
%  The two last arguments should be names of output files, of the
% hilbert series and the Poincare series, respectively.

(de multihilbert (infi1 infi2 outfi1 outfi2)
 (prog	(multiouth multioutpb oldoutch oldinch lastrel lastrelindex
	lastrelparts)
	(dskin infi1)
	(setq oldinch (rds (open infi2 'input)))
	(setq lastrelparts (read))
	(close (rds oldinch))
	(setq multiouth (open outfi1 'output))
	(setq multioutpb (open outfi2 'output))
	(copyd 'degreeenddisplay 'multided)
	(on custshow)
	(setq lastrelindex (initindex (length lastrelparts)))
  ml	(setq lastrel (index2pol lastrelindex lastrelparts))
	(setq oldoutch (wrs multiouth))
	(terpri) (terpri) (print lastrel) (terpri)
	(channelflush (wrs oldoutch))
	(lisppols2input (copy (cons lastrel multihilbertin)))
	(setq embdim 5)
	(groebnerinit)
	(groebnerkernel)
	(groebnerfinish)
	% Insert a conditional calctolimit.
	(clearall)
	(cond ((updateindex lastrelindex) (go ml)))
	(close multiouth)
	(close multioutpb)
))



	
