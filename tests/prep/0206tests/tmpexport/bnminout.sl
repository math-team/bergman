%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Copyright (C) 2001 Svetlana Cojocaru, Victor Ufnarovski
%%
%% Bergman is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY.  No author or distributor
%% accepts responsibility to anyone for the consequences of using it
%% or for whether it serves any particular purpose or works at all,
%% unless (s)he says so in writing.  Refer to the Bergman General
%% Public License for full details.

%% Everyone is granted permission to copy, modify and redistribute
%% bergman, but only under the conditions described in the
%% Bergman General Public License.   A copy of this license is
%% supposed to have been given to you along with bergman so you
%% can know your rights and responsibilities.  It should be in a
%% file named copyright.  Among other things, the copyright notice
%% and this notice must be preserved on all copies.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

(OFF RAISE)

%  Created 2001-08-03 to compute Betti numbers for right modules /SK & VU 

(GLOBAL '(NMODGEN NLMODGEN NRMODGEN bnmScInput   bnmInputFile  bnmOutFile
 anresScInput ANICKRESOLUTIONOUTPUTFILE))

(ON PBSERIES)

(DE bnmScreenInput ()
 (PROG	  (objtp)
          (SETQ bnmScInput T)
          (PRIN2 "Input the Maximum Degree you want to calculate")(TERPRI)
          (SETMAXDEG (READ))
          (COND ( (EQ (SETQ objtp (GETOBJECTTYPE)) 'MODULE)
		  (PRIN2 "Input the number of the module variables")(TERPRI)
		  (SETQ NMODGEN (READ)))
                ( (EQ objtp 'TWOMODULES)
		  (PRIN2 "Input the number of the right  module variables")
		  (TERPRI)
		  (SETQ NRMODGEN (READ))
		  (PRIN2 "Input the number of the left  module variables")
		  (TERPRI)
		  (SETQ NLMODGEN (READ))	)

          )%COND
          (PRIN2 "Now input all ring and then all module variables.") 
          (TERPRI)
	  (COND ((EQ objtp 'TWOMODULES) 
		 (PRIN2 "Right module variables should be before the left ones.")))
          (PRIN2 "Input  the ring ideal and module relations in algebraic form  thus:")
          (TERPRI) (PRIN2 "      vars v1, ..., vn;") (TERPRI)
          (PRIN2 "       r1, ..., rm;") (TERPRI)
          (PRIN2 "where v1, ..., vn are all the variables, and r1, ..., rm the  relations.")
          (TERPRI)
          (ALGFORMINPUT) ))


(DE bnmCheckInput (files)
  (COND ( (NOT  (FILEP (CAR files)) )
          (PRIN2 "***Input file must exist")  (TERPRI)
          (PRIN2 "Input data from the keyboard")  (TERPRI)
          (bnmScreenInput)
        )
        (T
           (SETQ bnmInputFile (CAR files))
           (SETQ bnmScInput NIL)
           (DSKIN bnmInputFile)
        )
  )
)

(DE bnmCheckOutput (files )
   (COND ( (NOT (STRINGP (CADR files)) )
           (PRIN2 "***Incorrect output file.") (TERPRI)
           (PRIN2 "Do you agree to use the file outf____.bn as output ?" )
           (TERPRI)
           (COND ( (YESP "Type Yes or No")
                   (SETQ bnmOutFile "outf____.bn")
                   (PRIN2 "outf____.bn is used as output file") (TERPRI)
                   (PRIN2 "Don't forget to rename it after calculations. " )
                   (TERPRI)
                )
                (T
                   (PRIN2 "No output file. Screen output") (TERPRI)
                         
                )
           )
         )
         (T (SETQ bnmOutFile (CADR files)))
   )
)



(DE anresScreenInput ()
          (SETQ anresScInput T)
          (PRIN2 "Input the Maximum Degree you want to calculate")(TERPRI)
          (SETMAXDEG (READ))
          (TERPRI)
          (PRIN2 "Now input in-variables and ideal generators in algebraic form, thus:")
          (TERPRI) (PRIN2 "     vars v1, ..., vn;") (TERPRI)
          (PRIN2 "      r1, ..., rm;") (TERPRI)
          (PRIN2 "where v1, ..., vn are the variables, and r1, ..., rm the generators.")
          (TERPRI)
          (ALGFORMINPUT)
)


(ON RAISE)


