
Here is (1-H)^(-1) for Hilbert series H of the module

   +1
   +2*t^1
  +10*t^2
  +45*t^3
 +204*t^4
 +928*t^5
+4222*t^6
Here is the Hilbert series H of the module
  +2*t^1
  +6*t^2
 +13*t^3
 +28*t^4
 +64*t^5
+149*t^6
