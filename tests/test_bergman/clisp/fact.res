% 2
-x*y+x^2,
   -a*x+x*a,
   -a*y+y*a,
   -b*x+x*b,
   -b*y+y*b,
   -b*a+a^2,
   -c*a+c*x,
   -c*b+c*y,
   X*c-c*x,
   Y*c-c*y,
   A*c-c*x,
   B*c-c*y,
   
% 3
c*x*a-c*x^2,
   -c*x*b+c*x^2,
   -c*y*x+c*x^2,
   c*y*a-c*x^2,
   c*y*b-c*y^2,
   
% 4
c*x^2*a-c*x^3,
   c*x^2*b-c*x^3,
   -c*y^2*x+c*x^3,
   c*y^2*a-c*x^3,
   c*y^2*b-c*y^3,
   
% 5
c*x^3*a-c*x^4,
   c*x^3*b-c*x^4,
   -c*y^3*x+c*x^4,
   c*y^3*a-c*x^4,
   c*y^3*b-c*y^4,
   
% 6
c*x^4*a-c*x^5,
   c*x^4*b-c*x^5,
   -c*y^4*x+c*x^5,
   c*y^4*a-c*x^5,
   c*y^4*b-c*y^5,
   
% 7
c*x^5*a-c*x^6,
   c*x^5*b-c*x^6,
   -c*y^5*x+c*x^6,
   c*y^5*a-c*x^6,
   c*y^5*b-c*y^6,
   
% 8
c*x^6*a-c*x^7,
   c*x^6*b-c*x^7,
   -c*y^6*x+c*x^7,
   c*y^6*a-c*x^7,
   c*y^6*b-c*y^7,
   
