[If you read this file you probably had downloaded the Bergman package
 and unpacked it in a directory which we will denote <Bergman root>.
 This file is <Bergman root>/scripts/clisp/install.doc]

1. Common Lisp resources and installation -----------------------------

First of all, you need Common Lisp installed at your machine.
We are using free Common Lisp implemented by Bruno Haible and
Michael Stoll at the Karlsruhe University, Germany.
The version should be 2.29 of 2002-01-29 or newer.
You can try (system::version) under CLISP to get the version date.

You can found all necessary links at
  http://www.clisp.org

In many cases, especially under UNIX, you are to contact your system
administrator to install any software, including CLisp.

If you do not find binaries for your platform, you should download sources
and compile them.

All files are packed to .zip or .tar.gz and contain all necessary
instructions. Would you see a README file in a directory, read it - it
contains important information!

2. If you have Common Lisp already ---------------------------------------

Go to <Bergman root>/scripts/clisp/<your platform>. You will find
there a file named makebergman or alike (mkbergm, mkbergm.bat, etc.).
Start this script (batch file) and follow instructions. ReadMe files,
if any, contain additional important informations and instructions.

CLisp-specific directories are:
  <Bergman root>/scripts/clisp/<your platform>/   - installation scripts and files
  <Bergman root>/bin/clisp/<your platform>/       - binary files after installation
  <Bergman root>/shells/clisp/<your platform>/    - shells, if any
  <Bergman root>/auxil/clisp                      - common installation stuff

At this moment there are two different platforms: unix and win9598nt.

A. General installation notes

   It is recommended to try the automatic installation procedure before
you'll begin with the manual configuration.

   The notes below apply to the manual configuration.

   It is very important to find the correct directory with the lisp
executable.

   Under normal UNIX installatiom it is something like /usr/local/bin/clisp.

B. Unix specific installation
   The installation script is written to work under Bourne shell (and all
derived shells like sh, ksh, bash, zsh). It tries to execute /bin/sh in
order to work. Some computers have a different location for this shell.
If it is your case you can simply run the command
<path-to-shell>sh mkbergman <options>

B.I. Fast start
 You can try the automatic installation procedure which will try to
guess the values of the parameters.
In this case you should first change the current directory to
<Bergman root>/scripts/clisp/unix  and run there the installation script as follows:

./mkbergman -auto [-i]

The optional parameter -i indicates that after guessing the values the
user will be prompted for confirmation of these values, with a possibility
to change them.

B.II. Manual configuration

The syntax of the installation script for manual configuration is:

./mkbergman <clisp-dir> [<bergman-root-dir> [<lisp-executable-file-name]]

First argument is a common lisp directory (see section A), the second
one is the BERGMAN root directory. The third argument is needed when
the common lisp executable has a different name (by default "clisp").

C. Microsoft Windows specific installation

   There are some specific stuff in installing under Windows.
The syntax for the installation program is:
makebr.bat <CLISP-dir>
where CLISP-dir is the same as discussed above. For example

makebr.bat c:\clisp_2_29

D. Notes for MSDOS users

   There is no installation script at this time. CLISP and bergman for MSDOS 
are mo more supported.

E. Other Common Lisp

There should be no big troubles installing this package under a
different Common Lisp implementation. The Common Lisp specific files are
in the <bmroot>/auxil/clisp and <bmroot>/scripts/clisp directories.
If you have any troubles or questions you can contact directly the
bergman port to Common Lisp developers at the address kae@math.md .

