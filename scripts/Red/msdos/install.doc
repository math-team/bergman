----------
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright (C) 1994,1995,1997 Joergen Backelin, Svetlana Cojocaru,
;;                          Alexander Kolesnikov, Victor Ufnarovski
;;
;; Bergman is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY.  No author or distributor
;; accepts responsibility to anyone for the consequences of using it
;; or for whether it serves any particular purpose or works at all,
;; unless he says so in writing.  Refer to the Bergman General Public
;; License for full details.

;; Everyone is granted permission to copy, modify and redistribute
;; bergman, but only under the conditions described in the
;; Bergman General Public License.   A copy of this license is
;; supposed to have been given to you along with bergman so you
;; can know your rights and responsibilities.  It should be in a
;; file named copyright.  Among other things, the copyright notice
;; and this notice must be preserved on all copies.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                 DOS. Installing BERGMAN under MS-DOS


DOS.1. Converting source files and documents.

     Your version of BERGMAN is uniform for many operating system.
All the files that are specific to MS-DOS are contained in the
\SCRIPTS\RED\MSDOS directory. Two of them bergm_sh.exe and  greb.exe are
binary files. All other files of BERGMAN  are text files
(sources and documents) and they are kept in UNIX style where the
end of line is represented by the LF character (dec. 10, hex. 0A).
MS-DOS uses CR-LF or CR (dec. 13-10 or 13, hex.  0D-0A or 0D) for
the purpose. If you had not converted files to MS-DOS style when
receiving, you can use the appropriate program to make it.  It is
necessary to be able to print or to edit them in MS-DOS.
For some more information about converting and decompressing programs see
the chapter DOS.5.

DOS.2. Tuning your REDUCE.

     To install BERGMAN you should have REDUCE version 3.4.1 or 3.5
(3.5 is preferable) and the corresponding version of PSL. Your REDUCE
should be properly installed and tuned. If you have REDUCE already installed,
read DOS.3.

DOS.2.1. Installing REDUCE with PSL.

     (The following is necessary only if you have not already installed
REDUCE. If you have both REDUCE 3.5 and full PSL, but have not installed
them together, you may however wish to do so.)

     When being installed REDUCE should necessarily be linked with the
full PSL. So the proper sequence is as follows:

     1. Install the full PSL.
     2. Add to your AUTOEXEC.BAT the command SET PSL=<path to PSL>,
e.g. SET PSL=C:\PSL
     3. Reload MS-DOS to activate this change in AUTOEXEC.BAT.
     4. Issue the command SET without parameters and check once more
existence and the value of the environment variable PSL.
     5. Now install REDUCE.

DOS.2.2. Setting directory for temporary files.

     If you had not set directory for REDUCE temporary files installing
your system, you should set it now. REDUCE uses the directory to allocate
virtual memory pages. This directory is to be situated on that
partition of your hard disk which has enough free space. By default,
REDUCE uses the current directory for virtual memory paging.

     This setting is described in the REDUCE Installation Guide.  (You
make the directory with REDUCE binaries the current one
(CD C:\REDUCE\BIN\DOS386 (3.5), or CD C:\REDUCE\BIN (3.4.1)) and issue
the following command:
386SETUP -k PSLL.EXE -PAGEFILE C:\
Here you specify C:\ (the root directory on the disk C:) as the REDUCE
working directory. You can use another directory instead of C:\.)

     If your REDUCE or BERGMAN session is aborted, you will see new
files in the  REDUCE working directory with names like BBADEACE
(without extensions). These files are deleted automatically in the case
of normal REDUCE termination, but after an abnormal one they remain in
the directory. You should periodically inspect the REDUCE working
directory and delete such files. Moreover, after abnormal termination
the invisible waste space may appear on the disk. During inspection
you should start CHKDSK /F (MS-DOS up to version 5) or SCANDISK
(version 6 and later) to resolve this problem. (Warning: due to
a known bug, CHKDSK in MS-DOS versions 4 and 5 in some rare circumstances
may destroy the information on the hard disk.  Replace your old MS-DOS
by the newest version 6.22 or later.)

DOS 2.3. Tuning SLAVE.EXE.

     This step is also described in the REDUCE Installation Guide. Now
we do not use the DOS interface in BERGMAN. It is supposed to became
necessary in future. (Either you should edit binary file SLAVE.EXE or
reassemble it from the source to make it include the proper full path
to COMMAND.COM, or to provide a copy of COMMAND.COM in the directory
C:\DOS.)

DOS 3. Installing BERGMAN.

DOS 3.1. Creating the BERGMAN directory structure and copying files.

  BERGMAN is allocated in corresponding directory
structure.  When copying the system, you should keep this structure and
corresponding file allocation in directories. 

     All specific files to install BERGMAN under REDUCE and MS-DOS are
containing in the \SCRIPTS\RED\MSDOS\ directory. 


DOS 3.2. Compiling BERGMAN.

     Make \SCRIPTS\RED\MSDOS subdirectory of the BERGMAN top level directory
the current one, e.g.,
C:
CD C:\BERGMAN\SCRIPTS\RED\MSDOS

     Start the MS-DOS batch file MKBERGM.BAT. It has 3 parameters:
MKBERGM d: bdir rpath
Here:
- d is the disk letter, and bdir is the main directory for the BERGMAN
  package starting from the disk root \ but not ending by \
- rpath is the full path to REDUCE including the disk letter.
E.g.,
MKBERGM C: \BERGMAN C:\REDUCE

     If you obtained the MS-DOS message "Out of environment space",
consult your system programmer. Probably you need to increase the
environment size in your C:\CONFIG.SYS file:
SHELL=C:\DOS\COMMAND.COM C:\DOS /E:1024 /P
(/E:1024 sets 1024 bytes as the environment size.) Also it may be
useful to delete some SET commands from your AUTOEXEC.BAT, or to delete
some directories from the PATH command, thus decreasing the environment
total size.

     After installing BERGMAN, check the existence, date and time
of the BERGMAN0.921 in the BERGMAN root directory. Then go to the subdirectory
\BIN\RED\MSDOS and check the existence, date and time of BR.BAT, BERGMAN.BAT
files. Go to the subdirectory \LOGS and view through files RMSDOSC.LOG,
RMSDOSCA.LOG and RMSDOSBT.LOG.

     You are in trouble, if during the installation you had seen on the
screen messages starting with four or more asterisks ('****'), or if you
had found such messages in the LOG files. (Messages starting with less then
four asterisks are warnings and may be ignored.)  If you had found such
messages or BERGMAN0.921 was not created, please examine carefully your
MS-DOS, PSL and REDUCE configurations and settings and try to install
BERGMAN once more. In the case of repeating trouble you may inform the
author by e-mail joeb@matematik.su.se . The information on successful
installation and using of the package would also be appreciated.

To run bergman with shell use
   bergman
to run bergman without shell use
   br

DOS 4. Running BERGMAN.

DOS 4.1. Tuning BR.BAT, BERGMAN.BAT.

     You can tune the batch files BR.BAT, BERGMAN.BAT to obtain more
(virtual) memory for BERGMAN. Two numbers you see in batches
(like 3000000 100000) are memory size and stack size in bytes.
See the REDUCE Installation Guide for
further information ('Tailoring REDUCE.BAT, SLAVE.EXE' section).

DOS 4.2. Entering and exiting BERGMAN.

     Run the batch file BR.BAT. You will see the REDUCE logo and
version number, and the REDUCE prompt. Type the following magic words:
lisp;!*lisp!_hook:=t;end;
and press [Enter]. You will see the BERGMAN logo and version, and
the LISP prompt. Now you are in LISP (PSL) and you can issue BERGMAN
commands and LISP statements.

     To exit BERGMAN, issue the LISP command (quit).  In cases of
abnormal termination, exiting LISP may require the command (QUIT) in
uppercase letters.

DOS 4.3. Breaking BERGMAN.

     You can break BERGMAN by Ctrl-C but it may not always work.

     You can use the LISP command (ON GC) to see information about
garbage collecting. It may be annoying, but having something to put
constantly on the screen increases the possibility of breaking with
Ctrl-C. (GC report shows memory sizes not in bytes, but in LISP 4-bytes
items.)

DOS.5
    If you have received  files in the UNIX format you need to
convert them into MS-DOS one. We recommend you to use FLIP which you can
obtain from the SimTel Software Repository:
     oak.oakland.edu (141.210.10.117) (USA)
          the root is:   /SimTel/msdos/
     archie.au (139.130.4.6) (Australia)
          the root is:   /micros/pc/oak/
     ftp.funet.fi (128.214.248.6) (Finland)
          the root is:   /pub/msdos/SimTel/
and many others sites (use the one nearest to you). You can FTP SimTel
instructions and site lists from the SimTel subdirectory filedocs/.

FLIP is freeware under a GNU license.

     Take flip1exe.zip from the SimTel subdirectory textutil/.

     Other useful programs to work with UNIX files in MS-DOS are:
tar315us.zip and tar4dos.zip from the SimTel subdirectory archiver/;
comp430d.zip and gzip124.zip from the SimTel subdirectory compress/;
uuexe525.zip from the SimTel subdirectory decode/;
pkz204g.exe from the SimTel subdirectory zip/.

     Do not forget to set binary mode to FTP .zip and .exe files.

     '1', '315', '430d', '124', '525', and '204g' in filenames are
version numbers and may be changed: e.g., if you do not find
comp430d.zip but you see comp431a.zip, it is the newest version.

     PKZ204G.EXE is the self-extracting archive containing ZIP archiver
(version 2.04g), if you had not it before. FLIP1EXE.ZIP contains the
utility to convert UNIX style texts with lines ending by LF to MS-DOS
style texts with lines ending by CR-LF and vice versa. Other programs
from the above collection permit you to unpack UNIX files working in
MS-DOS (tar, z, compress and gz), and UU/XX en/decode files.
PKZ204G.EXE is shareware, but you can use freeware GNUzip (GZIP124.ZIP)
to unpack .ZIP files. All files contain the corresponding
documentation.




