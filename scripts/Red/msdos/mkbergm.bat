@echo off
rem Copyright (C) 1996, 1997 Joergen Backelin, Svetlana Cojocaru,
rem                          Alexander Colesnicov, Victor Ufnarovski
rem Bergman is distributed in the hope that it will be useful,
rem but WITHOUT ANY WARRANTY.  No author or distributor
rem accepts responsibility to anyone for the consequences of using it
rem or for whether it serves any particular purpose or works at all,
rem unless (s)he says so in writing.  Refer to the Bergman General
rem Public License for full details.
rem
rem Everyone is granted permission to copy, modify and redistribute
rem bergman, but only under the conditions described in the
rem Bergman General Public License.   A copy of this license is
rem supposed to have been given to you along with bergman so you
rem can know your rights and responsibilities.  It should be in a
rem file named copyright.  Among other things, the copyright notice
rem and this notice must be preserved on all copies.
rem
if .%1==.? goto :HELP
if .%1==. goto :HELP
if .%2==. goto :HELP
if .%3==. goto :HELP
if not .%4==. goto :HELP
echo Press Ctrl-Break to stop this batch and issue the command
echo   MKBERGM ?
echo to obtain help, or
pause
if not exist %1%2\nul goto :DIRERR1
if not exist %3\nul goto :DIRERR2
set BMROOT=%1%2
set REDUCE=%3
set BMEXE=%BMROOT%\BIN\RED\MSDOS
set BMLOAD=%BMROOT%\LAP\RED\MSDOS
set BMSRC=%BMROOT%\SRC
set BMAUXIL=%BMROOT%\AUXIL
set BMMKDIR=%BMROOT%\SCRIPTS\RED\MSDOS
set BMDOMAINS=%BMROOT%\DOMAINS
set BMVERS=BERGMAN0.921
set BMLOG=%BMROOT%\LOGS
cd %BMEXE%
if exist bergman0.* del bergman0.*
if exist versmacr.sl del versmacr.sl
copy %BMAUXIL%\versmacr.sl versmacr.sl
for %%i in (mode macr cmp1 cmp2 lisp brg3) do if exist spec%%i.sl del spec%%i.sl
for %%i in (exe go cfg ini) do if exist bergm_sh.%%i del bergm_sh.%%i
for %%i in (exe err) do if exist greb.%%i del greb.%%i
for %%i in (ro bergman br) do if exist %%i.bat del %%i.bat
for %%i in (mode macr) do if exist %BMMKDIR%\set%%i.sl copy %BMMKDIR%\set%%i.sl spec%%i.sl
for %%i in (cmp1 cmp2) do if exist %BMMKDIR%\set%%i.sl copy %BMMKDIR%\set%%i.sl spec%%i.sl
for %%i in (lisp brg3) do if exist %BMMKDIR%\set%%i.sl copy %BMMKDIR%\set%%i.sl spec%%i.sl
if exist %BMLOAD%\alg2lsp.b del %BMLOAD%\alg2lsp.b
if exist alg2lsp.sl del alg2lsp.sl
copy %BMSRC%\alg2lsp.sl alg2lsp.sl
for %%i in (c ca bt) do if exist %BMLOG%\rmsdos%%i.old del %BMLOG%\rmsdos%%i.old
if exist %BMLOG%\rmsdosc.log rename %BMLOG%\rmsdosc.log rmsdosc.old
if exist %BMLOG%\rmsdosca.log rename %BMLOG%\rmsdosca.log rmsdosca.old

copy %BMMKDIR%\bergm_sh.exe bergm_sh.exe
copy %BMMKDIR%\bergm_sh.go bergm_sh.go
copy %BMMKDIR%\greb.exe greb.exe
copy %BMMKDIR%\greb.err greb.err
copy %BMMKDIR%\bergm_sh.ini bergm_sh.ini

copy %BMMKDIR%\ro.bat ro.bat
if exist %BMROOT%\bergman0.921 del %BMROOT%\bergman0.921
if exist compile.sl del compile.sl
if exist compan.sl del compan.sl
copy %BMAUXIL%\hook1 + %BMAUXIL%\compile.sl compile.sl
copy %BMAUXIL%\hook1 + %BMAUXIL%\compan.sl compan.sl
call ro compile.sl %BMLOG%\rmsdosc.log %3
call ro compan.sl %BMLOG%\rmsdosca.log %3
if exist %BMLOG%\rmsdosbt.log rename %BMLOG%\rmsdosbt.log rmsdosbt.old
copy %BMAUXIL%\hook1 + %BMAUXIL%\bmtop.sl bmtop.sl
call ro bmtop.sl %BMLOG%\rmsdosbt.log %3
move bergman0.921 %BMROOT%\bergman0.921

copy %BMMKDIR%\bergman1.bat bergman.bat
echo set REDUCE=%3>> bergman.bat
echo set BMROOT=%1%2>> bergman.bat
echo set BMEXE=%BMROOT%\BIN\RED\MSDOS>> bergman.bat
echo set BMLOAD=%BMROOT%\LAP\RED\MSDOS>> bergman.bat
echo set BMSRC=%BMROOT%\SRC>> bergman.bat
echo set BMAUXIL=%BMROOT%\AUXIL>> bergman.bat
echo set BMMKDIR=%BMROOT%\SCRIPTS\RED\MSDOS>> bergman.bat
echo set BMDOMAINS=%BMROOT%\DOMAINS>> bergman.bat
echo set BMVERS=BERGMAN0.921>> bergman.bat
echo set BMLOG=%BMROOT%\LOGS>> bergman.bat
copy /a bergman.bat + %BMMKDIR%\bergman2.bat

copy %BMMKDIR%\br1.bat br.bat
echo set REDUCE=%3>> br.bat
echo set BMROOT=%1%2>> br.bat
echo set BMEXE=%BMROOT%\BIN\RED\MSDOS>> br.bat
echo set BMLOAD=%BMROOT%\LAP\RED\MSDOS>> br.bat
echo set BMSRC=%BMROOT%\SRC>> br.bat
echo set BMAUXIL=%BMROOT%\AUXIL>> br.bat
echo set BMMKDIR=%BMROOT%\SCRIPTS\RED\MSDOS>> br.bat
echo set BMDOMAINS=%BMROOT%\DOMAINS>> br.bat
echo set BMVERS=BERGMAN0.921>> br.bat
echo set BMLOG=%BMROOT%\LOGS>> br.bat
copy /a br.bat + %BMMKDIR%\br2.bat

del ro.bat
goto :END
:HELP
echo.
echo This batch script creates the BERGMAN 0.921 package for MS-DOS
echo Usage (3 parameters):
echo   MKBERGM d: bdir rpath  , where d is the disk letter
echo                            and bdir is the main directory for
echo                            the package starting from the disk root \
echo                            but not ending by \, and rpath is the
echo                            path to REDUCE (incl. the disk letter).
echo E.g.:
echo   MKBERGM C: \BERGMAN C:\REDUCE
echo.
goto :END
:DIRERR1
echo.
echo The supposed BERGMAN directory %1%2 does not exist.
echo Installation aborted.
echo.
goto :END
:DIRERR2
echo.
echo The supposed REDUCE directory %3 does not exist.
echo Installation aborted.
echo.
goto :END
:END

