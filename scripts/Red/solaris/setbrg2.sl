%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Copyright (C) 1998,2004 Joergen Backelin
%%
%% Bergman is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY.  No author or distributor
%% accepts responsibility to anyone for the consequences of using it
%% or for whether it serves any particular purpose or works at all,
%% unless (s)he says so in writing.  Refer to the Bergman General
%% Public License for full details.

%% Everyone is granted permission to copy, modify and redistribute
%% bergman, but only under the conditions described in the
%% Bergman General Public License.   A copy of this license is
%% supposed to have been given to you along with bergman so you
%% can know your rights and responsibilities.  It should be in a
%% file named copyright.  Among other things, the copyright notice
%% and this notice must be preserved on all copies.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Added (temporarily) 2004-06-04: In this Reduce, the pp module
% does exist; and the prettyprinter is corrected in speclisp.sl; whence
% anyhow we could do the smart printsetup definition:

(off usermode) (on comp)
(de printsetup () (prettyprint (e!X!T!R!A!C!Tc!U!R!R!E!N!Tm!O!D!E!S)) )
(off comp)

% The Reduce Interface procedures should be available as a standard:
(begin)
in "$bmsrc/redif.red";
symbolic;
end;
