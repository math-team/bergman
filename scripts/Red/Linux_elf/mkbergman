#!/bin/csh
################################################################
## Copyright (C) 1994,1996,1998,2006 Joergen Backelin
##
## Bergman is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY.  No author or distributor
## accepts responsibility to anyone for the consequences of using it
## or for whether it serves any particular purpose or works at all,
## unless (s)he says so in writing.  Refer to the Bergman General
## Public License for full details.

## Everyone is granted permission to copy, modify and redistribute
## bergman, but only under the conditions described in the
## Bergman General Public License.   A copy of this license is
## supposed to have been given to you along with bergman so you
## can know your rights and responsibilities.  It should be in a
## file named copyright.  Among other things, the copyright notice
## and this notice must be preserved on all copies.
################################################################

pushd $2

## Set variables.
#rm -f shellenv bergman

cat >tmp1 <<EOspecsrc
setenv bmroot $2
setenv reduce $1
setenv MACHINE Linux_elf
EOspecsrc

## You must edit next line, if $bmauxil no longer is the same as $bmroot/auxil.
cat auxil/head1 tmp1 auxil/envtail4 >shellenv

source shellenv

## Clean up the exe directory, and move there.

##### RISKY!
rm tmp*
rm -f $bmexe/shellenv
mv shellenv $bmexe
cd $bmexe

echo \ Checking case conversion:
if ( ! ( -d $bmsrc || -x conv )) then
  echo \ I must create converted versions of the source files,
  echo but the converter executable file $bmexe/conv
  echo seems not to exist, or not to be executable.
  echo \ Please compile it from the source file
  echo $bmroot/kernel/conv.c,
  echo put the result in the right place, and/or make it
  echo executable. Then try mkbergman again.
  echo \ Alternatively, install bergman or just run the
  echo caseconv script for some other architecture, where the
  echo appropriate executable conv exists!
  exit
endif

if ( ! ( -d $bmsrc)) then
  echo Creating case converted source files ...
  $bmroot/auxil/caseconv $bmroot/src $bmroot/auxil $bmsrc $bmauxil ./conv
  echo ... done
endif

rm -f $bmauxil/fullversion
cat $bmauxil/fvstart $bmauxil/version $bmauxil/fvend > $bmauxil/fullversion

rm -f bergman* tmp* versmacr.sl
cp -p $bmauxil/versmacr.sl .

## Create the scripts.
cat>tmp2 <<EObergman
source $bmexe/shellenv
exec $reduce/elf_psl/bpsl -td 10000000 -f $bmexe/$bmvers.img
#exec $bmexe/$bmvers
EObergman

cat $bmauxil/head1 tmp2 >bergman

chmod +x bergman

## Set up the specific (context depending) extra source files.
## specmode, speclisp are mandatory; the others just ignored if non-existing.
rm -f specmode.sl specmacr.sl speccmp1.sl speccmp2.sl speclisp.sl
rm -f specbrg1.sl specbrg2.sl specbrg3.sl
cp -p $bmmkdir/setmode.sl specmode.sl
cp -p $bmmkdir/setmacr.sl specmacr.sl
cp -p $bmmkdir/setlisp.sl speclisp.sl
#cp -p $bmmkdir/setbrg1.sl specbrg1.sl
cp -p $bmmkdir/setbrg2.sl specbrg2.sl
cp -p $bmmkdir/setbrg3.sl specbrg3.sl
cp -p $bmmkdir/setcmp1.sl speccmp1.sl
cp -p $bmmkdir/setcmp2.sl speccmp2.sl

## alg2lsp.sl might not be able to run in some set-ups, since it uses
## table driven scanners.
rm -f $bmload/alg2lsp.b alg2lsp.sl
cp -p $bmsrc/alg2lsp.sl alg2lsp.sl

## You may have added patches.sl and/or local.sl.

## Set up auxiliary files.
rm -f $bmshells/mkprimefile.old
mv $bmshells/mkprimefile $bmshells/mkprimefile.old
cat $bmauxil/head3 $bmauxil/head2 $bmauxil/mkprimefile>$bmshells/mkprimefile
chmod a+x $bmshells/mkprimefile

## Compile the binaries.
rm -f comp*
cat $bmauxil/hook1 $bmauxil/compext_psl.sl > compext.sl
cat $bmauxil/hook1 $bmauxil/comphead_psl.sl $bmauxil/compile.sl $bmauxil/comptail_psl.sl > compile.sl
cat $bmauxil/hook1 $bmauxil/comphead_psl.sl $bmauxil/compan.sl $bmauxil/comptail_psl.sl > compan.sl
rm -f $bmlog/Linux_elfc*.old
mv $bmlog/rLinux_elfce.log $bmlog/rLinux_elfce.old
mv $bmlog/rLinux_elfc.log $bmlog/rLinux_elfc.old
mv $bmlog/rLinux_elfca.log $bmlog/rLinux_elfca.old
$reduce/reduce <compext.sl >$bmlog/rLinux_elfce.log
$reduce/reduce <compile.sl >$bmlog/rLinux_elfc.log
$reduce/reduce <compan.sl >$bmlog/rLinux_elfca.log
#rm -f comp*

## Create bergman
rm -f bmtop.sl
cat $bmauxil/hook1 $bmauxil/bmhead_psl.sl $bmauxil/bmtop.sl $bmauxil/bmtail_psl.sl >bmtop.sl
rm -f $bmlog/Linux_elfbt.old
mv $bmlog/rLinux_elfbt.log $bmlog/rLinux_elfbt.old
$reduce/reduce <bmtop.sl >$bmlog/rLinux_elfbt.log
#rm -f bmtop.sl

## Clean up, and return.
rm -f tmp*
popd
