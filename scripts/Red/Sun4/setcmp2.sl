%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Copyright (C) 1997 Joergen Backelin
%%
%% Bergman is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY.  No author or distributor
%% accepts responsibility to anyone for the consequences of using it
%% or for whether it serves any particular purpose or works at all,
%% unless (s)he says so in writing.  Refer to the Bergman General
%% Public License for full details.

%% Everyone is granted permission to copy, modify and redistribute
%% bergman, but only under the conditions described in the
%% Bergman General Public License.   A copy of this license is
%% supposed to have been given to you along with bergman so you
%% can know your rights and responsibilities.  It should be in a
%% file named copyright.  Among other things, the copyright notice
%% and this notice must be preserved on all copies.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%	Created 1997-01-31.

%	CHANGES:

%  Converted to lower cases./JoeB 1997-09-29

%  Made Reduce version adding standard forms coefficients.
% /JoeB 1998-06-23

   % For the creation of modular logarithm tables.

(faslout (mkbmpathexpand "$bmload/primaux"))
(dskin (mkbmpathexpand "$bmauxil/primaux.sl"))
(faslend)

(faslout (mkbmpathexpand "$bmload/sfcoeffs"))
(dskin (mkbmpathexpand "$bmsrc/sfcf.sl"))
(faslend)
