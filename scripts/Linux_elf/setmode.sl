%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Copyright (C) 1994,1997 Joergen Backelin
%%
%% Bergman is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY.  No author or distributor
%% accepts responsibility to anyone for the consequences of using it
%% or for whether it serves any particular purpose or works at all,
%% unless (s)he says so in writing.  Refer to the Bergman General
%% Public License for full details.

%% Everyone is granted permission to copy, modify and redistribute
%% bergman, but only under the conditions described in the
%% Bergman General Public License.   A copy of this license is
%% supposed to have been given to you along with bergman so you
%% can know your rights and responsibilities.  It should be in a
%% file named copyright.  Among other things, the copyright notice
%% and this notice must be preserved on all copies.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%	CREATED 1994-09-02.


%	CHANGES:

%  Converted to lower cases./JoeB 1997-09-29


%(set!-heap!-size 1700000)

(global '(!*psl!-mode	% Restricted or extended psl, alone or
			% as a reduce base.
	  !*sl!-mode	% Standard Lisp.
	  !*cl!-mode	% Common Lisp.
	  !*csl!-mode	% Codemist Standard Lisp.
	  !*reduce!-mode % Some lisp that is a base for Reduce.
	  !*unix!-mode	% Unix operating system
	  !*msdos!-mode % MsDos operating system.
))

(on psl!-mode)
(on sl!-mode)
(off cl!-mode)
(off csl!-mode)
(off reduce!-mode)
(on unix!-mode)
(off msdos!-mode)
