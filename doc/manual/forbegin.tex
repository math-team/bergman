\hspace{5cm} \mbox {``If easy of use was the only valid criterion}

\hspace{5cm} \mbox { people would stick to tricycles and never}

\hspace{5cm} \mbox { try bicycles''.}

\hspace{10cm} \mbox{ D.Engelbart}

\section{Preliminaries}
{\bf Bergman} is a system for computations in commutative and purely
non-commutative
algebra. It is mainly developed by J\"{o}rgen Backelin (Stockholm University).
 Some additional facilities are implemented in the 
framework of a
joint project ``Non-commutative computer algebra'' 
executed by the Department of Mathematics at the Stockholm
University in collaboration with the Lund University and
the Institute of Mathematics and Computer Science of the Academy of Science of 
Moldova in Chisinau. The project is supported by the Royal Swedish Academy of 
Sciences which is 
gratefully acknowledged. 

We would like to express our sincere gratitude to the 
first project leader and a faithful  {\bf bergman} user Prof. Jan-Erik Roos. 
 
We also thank other people participated the project since its beginning:
Alexander Podoplelov who took part in the developing of Anick resolution 
component, Sergey Verlan who took part in the elaboration of the Common Lisp 
version.
Our colleagues   Alexander Colesnicov and Ludmila Malahova are working on the 
project starting with 1994.  They have drawn up the Common Lisp version, the {\bf 
bergman} site and programmed two versions of shell:
the first one under MS DOS 
and the current one in Java. Section \ref{sec:shell} in this book is written together 
with them.

{\bf Bergman} is public domain software available  from the following address
http://servus.math.su.se/bergman. It is written in Standard Lisp, the Lisp dialect
underlying Reduce implementation. An alternative Common Lisp
 version is also supported for some platforms.


In principle, {\bf bergman} can be used on all platforms where
 Reduce, PSL, or CLISP are implemented.
We have implemented it on:
\begin{itemize}
 \item MS Windows 95 and later (CLISP);
 \item Linux on different machines (PSL, Reduce, CLISP);
 \item Sun Solaris for Sparc and Sun Blade (PSL, Reduce, CLISP);
 \item Dec Alpha under OSF and Linux (PSL, Reduce).
\end{itemize}




For  detailed information  about different versions of operating
systems and Reduce or Lisp releases, see  the installation guide.

{\bf Bergman} is far from a full computer algebra system. However, it
may be run under Reduce and in the commutative setting be treated as any 
Reduce module.

Using {\bf bergman} one can compute both for ideals and right modules:

\begin{itemize}
\index{Gr\"{o}bner basis}     \item Gr\"{o}bner basis
\index{Hilbert series}        \item Hilbert series
\index{Poincar\'{e} series}   \item Poincar\'{e} series
\index{Anick resolution}      \item Anick resolution
\index{Betti numbers}         \item Betti numbers
\end{itemize}

The last three features are destined to the graded non-commutative computations only.

One can find the  description of {\bf bergman}, including a demo version on
its home page.

Of course, the demo version offers only limited possibilities, but you
can try to solve your own problems.

Here we describe a version of {\bf bergman}, installed under Reduce, 
and working
in the Standard Lisp environment. In this installation the user can
use both Reduce and Lisp syntax. Nevertheless  most of the text is
valid for other installations too. The Reduce--oriented syntax
will be discussed in section \ref{reduce}.

For those who is unfamiliar with the Gr\"obner basis concept we refer to \ref{sec:Math-app}
for an elementary introduction in the subject.

\subsection{Starting a {\bf bergman}  session}

You can start a \index{bergman session} {\bf bergman} session by typing
{\bf bergman } followed by {\bf Enter}.
When you are successful in starting the {\bf bergman} session you will
see a prompt.
If the installation was under Reduce, the prompt (after some possible
messages about memory and version) may look like:

{\bf
4: }

Now you maybe want to switch to the \index{Lisp mode} Lisp--mode. (If you prefer to work
in the
\index{Reduce mode} Reduce--mode read section \ref{reduce} instead.) For this you simply  type
{\bf end; } and then press {\bf Return } key. You will see a new Lisp--prompt:

\begin{verbatim}

Entering LISP ...

Bergman 0.984, 14-Dec-2004 

1 lisp>

\end{verbatim}

(If your installation was without Reduce you are here from the very beginning).
Of course, the date and the version can be different -- it depends
 from the date
when {\bf bergman} was compiled on your computer.

Typically {\bf bergman} will print a prompt such as\\

{\bf 4 lisp$>$ }\\

at the beginning of the line you should enter. Whenever you
see a prompt, {\bf bergman} is waiting for you to enter new
commands.



The Common Lisp version starts directly:

\begin{verbatim}
  i i i i i i i    ooooo    o        ooooooo   ooooo   ooooo 
  I I I I I I I   8     8   8           8     8     o  8    8
  I  \ `+' /  I   8         8           8     8        8    8
   \  `-+-'  /    8         8           8      ooooo   8oooo
    `-__|__-'     8         8           8           8  8
        |         8     o   8           8     o     8  8
  ------+------    ooooo    8oooooo  ooo8ooo   ooooo   8

Copyright(c) Bruno Haible,Michael Stoll 1992, 1993
Copyright(c) Bruno Haible,Marcus Daniels 1994-1997
Copyright(c) Bruno Haible,Pierpaolo Bernardi,Sam Steingold 1998
Copyright(c) Bruno Haible,Sam Steingold 1999

Welcome to the BERGMAN system.

[1]> 

\end{verbatim}

Now you are ready for computations: $ [1]>$ is the input  prompt.
Lately we will not distinguish PSL and Common Lisp version and will use
the word Lisp for both of them.

\subsection{Ending a {\bf bergman}  session}

The command \index{quit} {\bf (quit)} followed by the {\bf Return } key, ends
  a {\bf bergman}  session.

\vspace{0.2cm}

{\bf Example.} {\it Here you finish the session.}

\vspace{0.2cm}

{\bf 10 lisp$>$ (quit) }\\
In Reduce syntax you omit the parenthesis but add a semicolon, thus:

{\bf 10: quit; }

\vspace{0.2cm}

An alternative solution on some platforms is to use Ctrl--D; holding down the Ctrl--key
while pushing one or several times  on key ``D'' will make quit. This
may work  in Reduce syntax, too.

\subsection{What you need to know about Lisp syntax}

The user should realise that {\bf bergman} is a Lisp program and whenever (s)he
starts {\bf bergman} (s)he works under Lisp and in the Lisp  notations. Here
we
describe
the necessary minimum of Lisp syntax to deal with {\bf bergman} in the simplest
cases.

First of all, all commands should be written within parenthesis -
see Example above.

It is important that uppercase and lowercase may be different in Lisp.
One can use, for instance,  only lowercase. Nevertheless in some situations,
arising from
mistakes, you leave {\bf bergman}, but still need to leave Lisp. In this case
{\bf (quit)} not necessary ends the session
and you need  to use 
\index{quit} {\bf (QUIT)} to do this.
% (and sometimes twice!).
In the later version of Reduce the situation is opposite: you are able
to use lowercase, but uppercase produces errors. Conclusion: try
both in troubles!

Note also that a typical mistake is to forget one of the right parenthesis or
quotes ({\bf '' }). So maybe a couple of them might be useful to leave
Lisp safely.

During the session you can get some kind of messages from Lisp.
All of them start with stars. The message that starts from five stars
$*****$ means an error. Three stars $***$ mean a minor error or
only a warning --
it is possible to continue the work.

\vspace{0.2cm}

{\bf Example.} {\it Here we forget to write the parenthesis.}

\vspace{0.2cm}

{\bf
2 lisp$>$ simple

***** `simple' is an unbound ID

3 lisp$>$
}

Sometimes the output from garbarge collection can irritate the user. 
The comand {\bf (off gc)} can help in this case.





\section{Simplest calculations in {\bf  bergman}}

 Here we describe and explain
 several examples that you can easily copy and modify.


The simplest way to employ {\bf bergman} is to start it, to use some
specially written routines
such as
\index{simple} {\bf simple} or \index{ncpbhgroebner} {\bf
  ncpbhgroebner,}  to
feed it input interactively or by means of an input file prepared in
advance, and to quit.

In a slightly more sophisticated use, you may
influence the behavior by various "mode changing" procedures.

In very sophisticated use, you may employ and expand the
experimental procedures enclosed to the program, and/or interact
directly with the underlying Lisp representations of the algebraic
objects.

You also have access to all source code and can use all procedures
to implement your own applications.

This chapter covers the first use.  For more
sophisticated use guidance see the next chapters.

\subsection{Selecting alternatives}

{\bf Bergman} works in different modes. You can find  a full overview of these
in \ref{refm}.

To perform some computations in {\bf bergman} it is necessary at least
to set up the polynomial ring selecting commutative or non-commutative
alternative,  \index{ordering} ordering \index{degrevlexify} ({\bf degrevlexify} or
\index{deglexify}  {\bf deglexify}),
\index{coefficients} coefficients field
(characteristic \index{characteristic}  0, 2 or arbitrary prime),
\index{weights} weights of variables etc.

For the first examples we  skip the complete description of alternatives
using the corresponding setting included in the main top level procedures.
We shall distinguish here only commutative and non-commutative calculations.

\subsection{Commutative algebras}



Let us start with an example of \index{Gr\"{o}bner basis} Gr\"{o}bner
basis computation for an ideal.
It will be performed by the procedure \index{simple} {\bf simple}. There are several
ways to call this procedure explained below.
Here we  illustrate two of them.

Calling {\bf simple} without arguments one can introduce the relations
directly from the screen following the prompt and respecting one
restriction: the relations must be homogeneous. An example of
the session follows:                

\begin{verbatim}
1 lisp> (simple)
Now input in-variables and ideal generators in 
algebraic form, thus:
      vars v1, ..., vn;
      r1, ..., rm;
where v1, ..., vn are the variables, 
and r1, ..., rm the generators.
algebraic form input> vars x,y;x^2-y^2,x*y;
% 2
x*y,
   x^2-y^2,

% 3
y^3,

Done
 - All is OK (I hope). Now you may (e. g.):
   - kill bergman with (QUIT); or
   - interrupt bergman with ^Z; or
   - clear the memory with (CLEARIDEAL), 
     and run a new (SIMPLE).
nil
2 lisp>
\end{verbatim}

The result contains three elements of Gr\"{o}bner basis : $xy, x^2-y^2, y^3.$
Note that, according to the order of variables, $x$ (the first in the list)
is highest, so $xx, xy$ and $yyy$ are highest monomials.
Thus, only the monomials $ 1, x, y, yy$ are normal (not divisible by the
highest monomials) and serve as a basis of our algebra. Its
dimension is equal to 4 and we can easily create a multiplication table too.
(All elements of the Gr\"{o}bner basis are equal to zero in the
quotient algebra, so we can
use their highest terms for the reduction of non-normal words).
For example, $x \cdot x=yy$, $y \cdot x=0$, $y \cdot yy=0$.

As was said above, \index{simple} {\bf simple} may be called in several ways. One
of them is to  perform input and output by means of  files.
Let us prepare the following one (suppose that its name is "test1.a".
Check its existence in the directory {\bf bergman/tests} and copy it
into your current directory):

{\bf
\index{algforminput} (algforminput)

vars x,y;

x$*$x$-$y$*$y, x$*$y;
}

The first line informs {\bf bergman} that the succeeding lines are
input data in the \index{algebraic form} algebraic form. It means that you need to write
multiplication symbol $*$ or powers for example
$ x\verb-^-2$ or $x**2$ instead of
 $x*x$ but not $xx$
 (the same conventions as in Reduce or Maple). The other possibility is
 \index{Lisp form} Lisp--form;
 read about them in the subsection \ref{inputform}.

 The next two lines are the input data themselves. The first one contains variables, they should be
 written between keyword \index{vars} {\bf vars} and semicolon. Then comes the
 defining relations,
 separated by commas and finished by semicolon.

 To start the calculation select the name for output file, for example
 "test1.bg"
 (it should not
 exists!), start {\bf bergman}, switch to the Lisp
 mode and write

 {\bf (simple "test1.a" "test1.bg")}

 (do not forget double quotes!) and then quit.

The following is the full session of our work.

\begin{verbatim}
1 lisp> (simple "test1.a" "test.bg")
t
 - All is OK (I hope). Now you may (e. g.):
   - kill bergman with (QUIT); or
   - interrupt bergman with ^Z; or
   - clear the memory with (CLEARIDEAL), 
     and run a new (SIMPLE).
nil
2 lisp> (quit)

Quitting
\end{verbatim}

Here is the resulting file "test1.bg", containing Gr\"{o}bner basis
\begin{verbatim}
% 2
x*y,
   x^2-y^2,

% 3
y^3,

Done


\end{verbatim}



\subsection{Non-commutative algebras}

The procedure \index{simple} {\bf simple} may be used to perform
non-commutative \index{Gr\"{o}bner basis} Gr\"{o}bner basis computations also.
{\bf Bergman} by default is in commutative mode, so,
first of all we need to turn it to non-commutative calculations.
Here is an example of the session:

\begin{verbatim}
2 lisp> (noncommify)
nil
3 lisp>  (simple)
Now input in-variables and ideal generators in 
algebraic form, thus:
      vars v1, ..., vn;
      r1, ..., rm;
where v1, ..., vn are the variables, 
and r1, ..., rm the generators.
algebraic form input> vars x,y;x^2-y^2,x*y;
% 2
x*y,
   -y^2+x^2,
   
% 3
x^3,
   y*x^2,
   
Done
 - All is OK (I hope). Now you may (e. g.):
   - kill bergman with (QUIT); or
   - interrupt bergman with ^Z; or
   - clear the memory with (CLEARIDEAL), 
     and run a new (SIMPLE).
nil
4 lisp> 


\end{verbatim}



Although variables and generators look the same as in the commutative case, we have, of course, different output of \index{Gr\"{o}bner basis}
Gr\"{o}bner basis.

According to the order of variables, $y$ (the last in the list --
opposite to the defaults in the commutative case)
is the highest, so $ xy$, $yy$, $xxx$ and $yxx$ are the highest monomials.
Thus only monomials $1$, $x$, $y$, $xx$, $yx$ are normal (do not contain the highest
monomials as subwords) and serve as a basis of our algebra. Its
dimension is equal to 5 and we can easily create a multiplication table too.
(All elements of the Gr\"{o}bner basis are equal to zero in algebra, thus
we can
use their highest terms for the reduction of non-normal words).
For example,  $ x \cdot x=xx$, $y \cdot y=xx$, $x \cdot xx=0$.

In the non-commutative case the homogeneity restriction also must be
respected (excepting ``the jumping rabbit'' strategy, see \ref{rabbit}).

The input can be performed also by means of a file.
Let us prepare the following one (suppose that its name is "test2.a".
Check its existence in the directory {\bf bergman/tests} and copy it
into your current directory):

\index{noncommify} {\bf (noncommify)

(setmaxdeg 10)}

\index{algforminput} {\bf (algforminput)

vars x,y;

x$*$x$-$y$*$y,x$*$y;
}

The first line switches {\bf bergman} to the non-commutative mode.
The second line is not necessary in this example. It restricts
calculations up to degree 10. Here calculations stops in degree 3
(as you will see later), but in general \index{Gr\"{o}bner basis}
Gr\"{o}bner basis might be
infinite so it is recommended to restrict the degree of calculations
(although {\bf bergman} will try to do them until the memory doesn't suffice).

The third line informs {\bf bergman} that the following are the input data in the
 \index{algebraic form} algebraic form. It means that you need to write multiplication
 symbol $*$ or powers,  for example $x\verb-^-2$ or
 $x**2$ instead of $ x*x$, but not $xx$
 (the same as in Reduce or Maple). Another possibility is
\index{Lisp form} Lisp--form; read about it in the section \ref{inputform}.

 The next two lines are input data themselves. The first contains variables, they should be
 written between keyword \index{vars} {\bf vars} and semicolon. Then the
 generators are listed,
 separated by commas and finished by a semicolon.

 To start the calculation select the name for output file,
 for example "test2.bg" (it should not
 exist!), for example "test2.bg", start {\bf bergman}, switch to the Lisp
 mode and write\\
{\bf (simple "test2.a" "test2.bg")}\\
(do not forget double quotes!) and then quit.

The following is the full session of our work.

\begin{verbatim}
1 lisp> (simple "test2.a" "test2.bg")
nil
nil
t
 - All is OK (I hope). Now you may (e. g.):
   - kill bergman with (QUIT); or
   - interrupt bergman with ^Z; or
   - clear the memory with (CLEARIDEAL), 
     and run a new (SIMPLE).
nil
2 lisp> 

\end{verbatim}


The file {\bf test2.bg} contains the corresponding \index{Gr\"{o}bner basis}
Gr\"{o}bner basis:

\begin{verbatim}
% 2
x*y,
   -y^2+x^2,
   
% 3
x^3,
   y*x^2,

Done
\end{verbatim}

Summing up our knowledges about the procedure {\bf simple} we can describe 
it now in a more formal way.

{\bf Simple} can  be applied both in the commutative and noncommutative
case. By default {\bf bergman} works in the commutative mode. To
turn off commutativity one need to call
\index{noncommify}{\bf (noncommify)}

To return to the commutative case  one should  call \index{commify}{\bf (commify)}

{\bf Simple} is called as a procedure with 0, 1, or 2 file names as
arguments (file names being given
in the explicit form.) If you give no argument, the procedure assumes
that you want to give input on--line in ''algebraic form'' (see the section
\ref{inputform}) and prompts
you for this. If you give arguments, and the first one is the
name of an existing file, this is read, and it is assumed that it
(inter alia) contains the input (in one or another form).  If file
does not exist, the procedure works as if there were 0 names. Note,
that if input file contains {\bf (noncommify)} or {\bf (commify),}
the procedure
will work in the corresponding mode (and save it after returning).

 If there
is a second argument, the output is put there. However, if there is
an existing file with this name, it is NOT overwritten and the
procedure informs you about its existence and finishs its work
without doing something else.

There exists a related procedure {\bf stagsimple} \index{stagsimple} which 
works similarly, but it  uses a different algorithm of calculations, namely
the Staggered linear basis
Algorithm With Substance (SAWS). It thus may only be used in the
commutative case. It takes 0, 1, 2, or 3 arguments; the last two
are for output of the SAWS. deduced Gr\"{o}bner basis and for the
reduced Gr\"{o}bner basis, respectively. In some cases it works
more efficient than {\bf simple}.


In the next example we use another procedure, working with 
non-com\-mu\-ta\-ti\-ve algebras only and calculating besides the Gr\"{o}bner basis
of the algebra the Hilbert and  \index{Poincar\'{e} series}
Poincar\'{e} series for the corresponding monomial algebra (see \ref{sec:Math-app}). 

There is no screen input, we should use files only.
We can use the same {\bf test2.a}
for input and {\bf test2.bg } for output (supposing that the output file
does not exist. If there is such file in your current directory remove
or rename it) and want to get two new files: {\bf test2.hs} for
the
\index{Hilbert series} Hilbert series and {\bf test2.pb} for the Poincar\'e series. 
The procedure has
the name \index{ncpbhgroebner} {\bf ncpbhgroebner} (from NonCommutative Poincar\'e--Betti and
Hilbert series)
and it always
has 4 parameters. Here is a session (messages can be different):
\newpage
\begin{verbatim}
1 lisp>(ncpbhgroebner "test2.a""test2.bg""test2.pb""test2.hs")
*** I turn on noncommutativity
nil
10
nil
*** Function `degreeenddisplay' has been redefined

% No. of Spolynomials calculated until degree 2: 0
% No. of ReducePol(0) demanded until degree 2: 0
% Time: 425

% No. of Spolynomials calculated until degree 3: 2
% No. of ReducePol(0) demanded until degree 3: 0
% Time: 646

% No. of Spolynomials calculated until degree 4: 8
% No. of ReducePol(0) demanded until degree 4: 5
% Time: 833
*** Function `degreeenddisplay' has been redefined
nil
2 lisp>
\end{verbatim}

The file "test2.hs" for \index{Hilbert series} Hilbert series looks now as:

\begin{verbatim}
+2*z^2
+0*z^3
+0*z^4
\end{verbatim}

(note that the known from the very beginning 
part \verb- 1+2*z- is absent here),
and the file {\bf "test2.pb"} for the monomial \index{Poincar\'{e} series}
Poincar\'{e} series looks as:



\begin{verbatim}
+t^2*(2*z^2)
+t^3*(2*z^2+2*z^3)
+t^4*(6*z^3+2*z^4)
\end{verbatim}

and also does not contain the first terms \verb- 1+t*(2*z).-

Note also that neither series contains terms in degree more
than 4 -- the last degree where {\bf bergman} have done some calculations.
Look to the section \ref{Hilbertseries} if you need more terms.

%But you will be especially surprised investigating  "test2.bg."
%It looks now as:

%begin{verbatim} (the slash before begin and end were removed!!)
% 2
%((1 1 2))
%((-1 2 2) (1 1 1))

% 3
%((1 1 1 1))
%((1 2 1 1))

%Done
%end{verbatim}

%It means that the result was written in the \index{Lisp form} Lisp--form. You %can try to
%understand it (it is the same as before -- try to recognize it!) or
%read more
%about this in the subsection \ref{inputform}, but let us
%now change the input file a little and name it "test3.bg."

%\begin{verbatim}
%(noncommify)
%(setmaxdeg 10)
%(setq algoutmode 'ALG)
%(algforminput)
%vars x,y;
%x*x-y*y,x*y;
%\end{verbatim}

%The session now looks almost the same (but the message about \index{algebraic
%form} algebraic form appeared):

%\begin{verbatim}

%1 lisp> (NCPBHGROEBNER "test3.a" "test3.bg" "test3.pbs" "test3.hs")
%*** I turn on noncommutativity
%nil
%10
%alg
%nil
%*** Function `degreeenddisplay' has been redefined

% No. of Spolynomials calculated until degree 2: 0
% No. of ReducePol(0) demanded until degree 2: 0
% Time: 442

% No. of Spolynomials calculated until degree 3: 2
% No. of ReducePol(0) demanded until degree 3: 0
% Time: 629

% No. of Spolynomials calculated until degree 4: 8
% No. of ReducePol(0) demanded until degree 4: 5
% Time: 833
%*** Function `degreeenddisplay' has been redefined
%nil
%2 lisp>

%\end{verbatim}

The file "test2.bg" is the same as "test2.bg" in the
example with {\bf simple}:

\begin{verbatim}

% 2
x*y,
   -y^2+x^2,
   
% 3
x^3,
   y*x^2,

Done
\end{verbatim}

Now we give a formal description of this procedure.

{\bf Ncpbhgroebner} \index{ncpbhgroebner} always takes 4 arguments, which should 
evaluate to file names.
 The first file is the input file (which must exist).
 The second one  will be the Gr\"{o}bner basis output file. It must {\bf not
exist} before the call to {\bf ncpbhgroebner.}
 On the third and fourth files the double \index{Poincar\'{e} series}
 Poincar\'{e}-Betti series and
the \index{Hilbert series} Hilbert series of the associated monomial ring 
will be output.
Existing files are overwritten. The output will be done degree by
degree, whence you may read partial results while the calculations
continue (and interrupt the calculations without losing the lower
degree results). Note that the ring and its associated monomial
ring have the same Hilbert series, while the double Poincar\'{e}-Betti
series only fulfill a termwise inequality; due to the existence of
a certain spectral sequence, the coefficients in the  Poincar\'{e}-Betti series
of the associated ring can never be less than the corresponding
coefficients for the `true' ring.

A related procedure is {\bf ncpbh}. \index{ncpbh} The only difference 
with the previous one consists in the absence of output file for the Gr\"{o}bner basis.
So, the computations are the same, but it takes only 3 arguments.

%(read more in the Chapter
%\ref{MATH-APP} about mathematical details).


\subsection {Normal form and reduction}

The main idea to use Gr\"{o}bner basis is to have a possibility to reduce 
a given
element $u$ to its normal form. {\bf Bergman} suggests
a simple procedure named \index{readtonormalform}  {\bf readtonormalform} which interactively 
asks an input for a desired polynomial and prints its normal  form - 
the result of the reduction. Let us consider a small example.
Suppose that we want to check if two elements $a^3$ and $b^3$ commute
in the non-commutative algebra $A=<a,b|2a^2-3b^2>.$
The way to do it is the following:\\

\vspace*{0.4cm}

A) Calculate Gr\"{o}bner basis:

\begin{verbatim}

2 lisp> (noncommify)
nil
3 lisp> (simple)
Now input in-variables and ideal generators in 
algebraic form, thus:
        vars v1, ..., vn;
        r1, ..., rm;
where v1, ..., vn are the variables, 
and r1, ..., rm the generators.
algebraic form input> vars a,b; 2*a^2-3*b^2;
SetupGlobals
 ... done
+t^2*(z^2)
% 2
-3*b^2+2*a^2,

+t^3*(z^2+z^3)
% 3
-b*a^2+a^2*b,

+t^4*(z^3+z^4)
Done
 - All is OK (I hope). Now you may (e. g.):
   - kill bergman with (QUIT); or
   - interrupt bergman with ^Z; or
   - clear the memory with (CLEARIDEAL),
     and run a new (SIMPLE).
nil
4 lisp>

\end{verbatim}

B) Check the commutator $a^3*b^3-b^3*a^3:$
\newpage

\begin{verbatim}

4 lisp> (readtonormalform)
algebraic form input> a^3*b^3-b^3*a^3;
is reduced to
2/3*(-a^4*b*a+a^5*b),
nil

\end{verbatim}

We see that the result (the normal form of the commutator) is nonzero,
so, the elements are not commuting. Moreover, we know exactly how 
far from zero the commutator is. The same computation 
with $a^4$ and $b^2$ gives us a different result:

\begin{verbatim}
5 lisp> (readtonormalform)
algebraic form input> a^4*b^2-b^2*a^4;

is reduced to
0,
\end{verbatim}

and we can conclude that those elements  are commuting. More generally, 
the procedure {\bf readtonormalform} can be used for the equality 
test: $u=v$ in our factor-algebra if and only if their difference 
is reduced to zero.

To be able to use the normal form in his own programs one can apply the 
following procedures:

\begin{itemize}

\item \index{readpol} {\bf (readpol l), } which reads the list $l$ of
 polynomials from the 
input, separated by the semicolons,

\item \index{writepol}{\bf (writepol l), } which prints them on the screen,


\item \index{reducepol}{\bf (reducepol a b), } which reduces the 
polynomial $a$ to the 
(printable) polynomial $b$,


\item \index{printqpols} {\bf (printqpols b), } which prints (printable) polynomial.

\end{itemize}

Note the difference between inner form of the polynomial, which normally is 
unprintable and external, printable form.

\subsection {Starting a new calculation}\label{starting}


We hope that your first experiments with {\bf bergman} were successful and
following the prompt after calculations you can:

\begin{itemize}
\item  kill {\bf  bergman} with {\bf(quit)}; or
\item  interrupt {\bf  bergman}  with \verb-  ^Z -; or
\item  clear the memory with  {\bf(clearideal)}, and run a new  {\bf(simple)}.
\end{itemize}

Presuming you would like to run a new computation 
let us explain more carefully what  the function  \index{clearideal}
{\bf clearideal}  is doing.

According to its name it does not clear all that was done before, but
only clear memory from the ideal generators and results of the previous 
calculations.

You always {\it should} call this function before starting a new cycle of
the calculations. The only exclusion is when you want to add some
new elements to the already calculated Gr\"{o}bner basis or use the  
Gr\"{o}bner basis for reduction, but for doing this kind of
stuff you should be a professional. So, once again, in this chapter:
{\em before the calling at the second time one of the top--of--the--top 
procedures, such
as} {\bf simple, ncpbhgroebner} {\em always call} {\bf clearideal} 
(or {\bf clearring}, see below.)

You need not do it from the very beginning, but you need to know
what it really clears. {\em It clears:}
 \begin{itemize}
 \item initial ideal generators,
 \item calculated Gr\"{o}bner basis,
 \item all the memory, used for the calculations.
 \end{itemize}

{\em  It saves:}
   \begin{itemize}
 \item all selected modes (see section \ref{modes}), including
    list of input variables.
  \end{itemize}



You can use this possibility: {\em do not introduce the same set of variables,
skipping } {\bf vars \ldots}

 

\vspace{0.4cm}

{\bf Example.} {\it Several computations in the same polynomial ring with
 different ideals.}


\begin{verbatim} 
1 lisp> (simple)
Now input in-variables and ideal generators 
in algebraic form, thus:
        vars v1, ..., vn;
        r1, ..., rm;
where v1, ..., vn are the variables, 
and r1, ..., rm the generators.
algebraic form input>  vars x,y,z;
algebraic form input> x^3, x^2*y-y^2*x, z^2*x, x^2*z-y^2*z;
% 3
x*z^2,
   x^2*z-y^2*z,
   x^2*y-x*y^2,
   x^3,
   
% 4
y^2*z^2,
   y^3*z,
   x*y^2*z,
   x*y^3,
   
Done
 - All is OK (I hope). Now you may (e. g.):
   - kill bergman with (QUIT); or
   - interrupt bergman with ^Z; or
   - clear the memory with (CLEARIDEAL), 
     and run a new (SIMPLE).
nil
2 lisp> (clearideal)
nil
3 lisp> (simple)
Now input in-variables and ideal generators 
in algebraic form, thus:
        vars v1, ..., vn;
        r1, ..., rm;
where v1, ..., vn are the variables, 
and r1, ..., rm the generators.
algebraic form input> y*z^2-z^3, x^2*z-y*z^2, x*y*z;
% 3
y*z^2-z^3,
   x*y*z,
   x^2*z-z^3,
   
% 4
z^4,
   x*z^3,
   
Done
 - All is OK (I hope). Now you may (e. g.):
   - kill bergman with (QUIT); or
   - interrupt bergman with ^Z; or
   - clear the memory with (CLEARIDEAL), 
     and run a new (SIMPLE).
nil
4 lisp> 

\end{verbatim}



A more powerful function is \index{clearring} {\bf clearring} which clears also the
list of input and output variables and their weights. Depending how you plan 
to continue calculations you can select one of the clearing functions. 

Note that both of them do not clear all the selected alternatives,  
a part of settings being kept even after their applying. 
One can see how to avoid troubles caused by this situation reading the section \ref{debugg}.
 


\subsection {Working safely  in low degrees}

  It is supposed by default that a minimal degree where the user
starts his calculations is at least $2$. It gives some possibilities for the optimisation and, in fact,  it is not a real restriction, because it is easy to remove linear relations decreasing the number of variables. But sometimes the user may have linear or constant terms in relations (e.g. to solve a system of linear equations or working in the non-graded case). For those cases there is a special mode, which can be achieved
 by long-named command \index{setsafelowtermshandling} {\bf (setsafelowtermshandling)}. To restore the the initial situation one can use the call\index{setquicklowtermshandling}
 {\bf (setquicklowtermshandling)}.

\subsection {Some useful advises}
Working in LISP the user can find some difficulties.
A simple error as a forgotten parenthesis can lead to  dramatical results
with the message like 

\begin{verbatim} 
***** Segmentation Violation
Break loop
5 lisp break (1) > 
\end{verbatim}

Normally it means that the calculation was unsuccessful. Most probably the input was wrong, or was done in the wrong mode setting (e.g. wrong ring: trying non-commutative performance in the commutative situation). One can  leave safely the break situation using Ctrl-D key and even try to call the procedure again in the correct mode (e.g. using {\bf (clearring)} for clearing the ring and {\bf (noncommify)} for making it non-commutative). Note that one can copy old inputs by mouse. It is a good idea to have inputs into a separate file - it can save time, because it is easier to copy lines from this file than from the previous lines. 

When the volume of the calculations is really huge the system could finish the work with the message 
\begin{verbatim}
 ``***** heap space low''
\end{verbatim}
 and in this case one may try to increase the memory for  {\bf bergman}. How to do it depends on platform. For example
when the user calls  {\bf bergman} in Unix under PSL he uses in fact the following command inside

\begin{verbatim}
 exec /usr/.../bpsl -td 10000000 -f /home/.../bin/Linux_elf/bergman.img
\end{verbatim}
 (we skipped the current names of the directories). Copying the corresponding exe-file bergman to a new one (e.g longbergman) and changing $-td 10000000$   to  $-td 80000000$ one can increase the memory 8 times! In Common Lisp the corresponding change will looks as a replacement of  $-m 50M$ to $-m 400M$  (if the system allows such a memory). 








