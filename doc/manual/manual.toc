\contentsline {chapter}{\numberline {1}A brief bergman tutorial}{7}
\contentsline {section}{\numberline {1.1}Preliminaries}{8}
\contentsline {subsection}{\numberline {1.1.1}Starting a {\bf bergman} session}{9}
\contentsline {subsection}{\numberline {1.1.2}Ending a {\bf bergman} session}{11}
\contentsline {subsection}{\numberline {1.1.3}What you need to know about Lisp syntax}{11}
\contentsline {section}{\numberline {1.2}Simplest calculations in {\bf bergman}}{12}
\contentsline {subsection}{\numberline {1.2.1}Selecting alternatives}{12}
\contentsline {subsection}{\numberline {1.2.2}Commutative algebras}{13}
\contentsline {subsection}{\numberline {1.2.3}Non-commutative algebras}{15}
\contentsline {subsection}{\numberline {1.2.4}Normal form and reduction}{20}
\contentsline {subsection}{\numberline {1.2.5}Starting a new calculation}{23}
\contentsline {subsection}{\numberline {1.2.6}Working safely in low degrees}{25}
\contentsline {subsection}{\numberline {1.2.7}Some useful advises}{26}
\contentsline {chapter}{\numberline {2}Computations in bergman}{27}
\contentsline {section}{\numberline {2.1}Introduction}{28}
\contentsline {section}{\numberline {2.2}{\bf Bergman}: philosophy and approach}{28}
\contentsline {section}{\numberline {2.3}{\bf Bergman}: main restrictions}{28}
\contentsline {section}{\numberline {2.4}The polynomial ring set up}{29}
\contentsline {subsection}{\numberline {2.4.1}Variable names and the flag {\bf raise} }{29}
\contentsline {subsection}{\numberline {2.4.2}Ordering}{31}
\contentsline {subsection}{\numberline {2.4.3}Using eliminating ordering}{33}
\contentsline {subsection}{\numberline {2.4.4}Matrix Ordering}{35}
\contentsline {subsection}{\numberline {2.4.5}Coefficients: background}{37}
\contentsline {subsection}{\numberline {2.4.6}Coefficients: choices}{38}
\contentsline {subsection}{\numberline {2.4.7}Weights handling}{40}
\contentsline {subsection}{\numberline {2.4.8}Rings exchange}{42}
\contentsline {section}{\numberline {2.5}Homogenisation}{44}
\contentsline {section}{\numberline {2.6}Choosing a strategy}{46}
\contentsline {section}{\numberline {2.7}Input--output modes for the polynomials}{47}
\contentsline {subsection}{\numberline {2.7.1}Commutative case}{47}
\contentsline {subsection}{\numberline {2.7.2}Non--commutative case}{48}
\contentsline {section}{\numberline {2.8}Calculating and using series}{49}
\contentsline {subsection}{\numberline {2.8.1}Hilbert series computation}{49}
\contentsline {subsection}{\numberline {2.8.2}Computation of Gr\"{o}bner basis using known\\ Hilbert series}{56}
\contentsline {section}{\numberline {2.9}The jumping rabbit}{59}
\contentsline {section}{\numberline {2.10}Stability and Non-graded Algebras}{61}
\contentsline {section}{\numberline {2.11}Some bricks to build your own procedures}{63}
\contentsline {section}{\numberline {2.12}Modules over non-commutative algebras}{66}
\contentsline {subsection}{\numberline {2.12.1}Gr\"{o}bner basis for modules over\\ non-commutative algebras}{68}
\contentsline {subsection}{\numberline {2.12.2}Hilbert series for modules over\\ non-commutative algebras}{69}
\contentsline {subsection}{\numberline {2.12.3}The Anick resolution and Betti numbers for the tri\discretionary {-}{}{}vi\discretionary {-}{}{}al module}{75}
\contentsline {subsection}{\numberline {2.12.4}Writing own procedures}{79}
\contentsline {subsection}{\numberline {2.12.5}The Anick resolution and Betti numbers for a right module}{82}
\contentsline {subsection}{\numberline {2.12.6}The minimal resolution and Betti numbers for a right module}{87}
\contentsline {subsection}{\numberline {2.12.7}The Anick resolution and Betti numbers for right, two-sided ideals, and factor-algebras, considered as right modules}{92}
\contentsline {subsection}{\numberline {2.12.8}Working with the left modules}{98}
\contentsline {subsection}{\numberline {2.12.9}Betti numbers for two modules}{104}
\contentsline {subsection}{\numberline {2.12.10}Calculating Hochschild homology of an algebra}{109}
\contentsline {section}{\numberline {2.13}Bergman under Reduce}{116}
\contentsline {subsection}{\numberline {2.13.1}Working in the Reduce syntax}{116}
\contentsline {subsection}{\numberline {2.13.2}Parametric coefficients}{118}
\contentsline {section}{\numberline {2.14}Debugging in {\bf bergman}}{119}
\contentsline {section}{\numberline {2.15}Customising bergman}{127}
\contentsline {section}{\numberline {2.16}Computations in bergman under shell}{131}
\contentsline {subsection}{\numberline {2.16.1}Problems of interface to {\bf bergman}}{131}
\contentsline {subsection}{\numberline {2.16.2}Shell overview}{133}
\contentsline {subsection}{\numberline {2.16.3}Shell description}{134}
\contentsline {chapter}{\numberline {3}Bergman for the advanced user}{139}
\contentsline {section}{\numberline {3.1}References}{140}
\contentsline {subsection}{\numberline {3.1.1}Mode handling}{140}
\contentsline {subsection}{\numberline {3.1.2}Sustained operating mode alternatives}{142}
\contentsline {subsection}{\numberline {3.1.3}The mode designator tree structure}{144}
\contentsline {subsection}{\numberline {3.1.4}The mode handling procedures}{147}
\contentsline {section}{\numberline {3.2}Monomials and polynomials in {\bf bergman} }{157}
\contentsline {section}{\numberline {3.3}Controlling the calculations process}{165}
\contentsline {section}{\numberline {3.4}The commutative Hilbert series procedures}{176}
\contentsline {section}{\numberline {3.5}User available flags}{180}
\contentsline {section}{\numberline {3.6}User available counters}{185}
\contentsline {chapter}{\numberline {4}Some useful appendixes}{187}
\contentsline {section}{\numberline {4.1}Bergman proceedings extending\\ Standard Lisp}{188}
\contentsline {subsection}{\numberline {4.1.1}Introduction}{188}
\contentsline {subsection}{\numberline {4.1.2}Fast variant Standard Lisp extensions}{188}
\contentsline {subsection}{\numberline {4.1.3}General Standard Lisp extensions}{189}
\contentsline {section}{\numberline {4.2}Formal description of bergman}{196}
\contentsline {subsection}{\numberline {4.2.1}Organisation of the programme source files}{196}
\contentsline {subsection}{\numberline {4.2.2}Overview of the (main) units and source files}{197}
\contentsline {subsection}{\numberline {4.2.3}Basic structures and naming conventions}{199}
\contentsline {subsection}{\numberline {4.2.4}Global structures and programme outline}{204}
\contentsline {section}{\numberline {4.3}Mathematical background}{207}
