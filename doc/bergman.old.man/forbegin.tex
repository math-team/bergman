

\section{Preliminaries}
{\bf Bergman} is a system for computations in commutative and purely
non-commutative
algebra. It is mainly developed by J\"{o}rgen Backelin (Stockholm University).
Some additional facilities are implemented by participants of joint
project ''Non-commutative computer algebra'' (Stockholm University,
Institute of Mathematics of Moldavian Academy of Sciences).

{\bf Bergman} is public domain software available by anonymous ftp from
ftp.matematik.su.se. It is written in Standard Lisp, the Lisp dialect
underlying Reduce implementation.

{\bf Bergman} presently runs on the following platforms with
pre--installed Reduce or PSL.

\begin{itemize}
\item Sun--Sparc station  under SunOS, Solaris and Linux;
\item Alpha station under Dec OSF and Linux;
\item PC under MS DOS;
\end{itemize}

The detailed information  about different versions of operating
systems and Reduce or Lisp releases see in the installation guide.

Using {\bf bergman} one can compute both for ideals and right modules:

\begin{itemize}
\index{Gr\"{o}bner basis}     \item Gr\"{o}bner basis
\index{Hilbert series}        \item Hilbert series
\index{Poincar\'{e} series}   \item Poincar\'{e} series
\index{Anick resolution}      \item Anick resolution
\index{Betti numbers}         \item Betti numbers
\end{itemize}

The last three features are destined to non-commutative computations only.

Here we describe a version of {\bf bergman}, installed under Reduce and working
in the Standard Lisp environment. In this installation the user can
use both Reduce and Lisp syntax. Nevertheless  most of the text is
valid for other installations too. The Reduce--oriented syntax
will be discussed in section \ref{reduce}.

\subsection{Starting a {\bf bergman}  session}

You can start a \index{bergman session} {\bf bergman} session by typing
{\bf bergman } followed by {\bf Enter}.
When you are successful in starting the {\bf bergman} session you will
see a prompt.
If the installation was under Reduce, the prompt (after some possible
messages about memory and version) looks as:

{\bf

4: }

Now you maybe want to switch to the \index{Lisp mode} Lisp--mode. (If you prefer to work
in the
\index{Reduce mode} Reduce--mode read section \ref{reduce} instead.) For this you simply  type
{\bf end; } and then press {\bf Return } key. You will see a new Lisp--prompt:

{\bf Entering LISP ...

Bergman 0.922,  9-Jun-98

1 lisp$>$ }

(If your installation was without Reduce you are here from the very beginning).
Of course, the date and the version can be different -- it depends
 from the date
when {\bf bergman} was compiled on your computer.

Typically {\bf bergman} will print a prompt such as\\
{\bf 4 lisp$>$ }\\
at the beginning of  line you should enter. Whenever you
see a prompt, {\bf bergman} is waiting for you to enter new
commands.




\subsection{Ending a {\bf bergman}  session}

The command \index{quit} {\bf (quit)} followed by the {\bf Return } key, ends
  a {\bf bergman}  session.
\begin{exm} \label{exquit} Here you finish the session
\end{exm}

{\bf 10 lisp$>$ (quit) }

In Reduce syntax you omit the parenthesis but add a semicolon, thus:

{\bf 10: quit; }

An alternative solution is to use Ctrl--D: holding down the Ctrl--key
while pushing one or several times  on key ``D'' will make quit. This
works  in Reduce syntax too.

\subsection{What you need to know about Lisp syntax}

The user should realise that {\bf bergman} is a Lisp program and whenever he
starts {\bf bergman} he works under Lisp and in Lisp  notations. Here
we
describe
the necessary minimum of Lisp syntax to deal with {\bf bergman} in the simplest
cases.

First of all, all commands should be written within parenthesis -
see Example \ref{exquit}.

It is important that uppercase and lowercase may be different in Lisp.
Normally it is  use only lowercase. Nevertheless in some situations,
arising from
mistakes you leave {\bf bergman}, but still need to leave Lisp. In this case
usual,
{\bf (quit)} not necessary ends the session
and you need  to use 
\index{quit} {\bf (QUIT)} to do this (and sometimes twice!).
In the later version of Reduce the situation is opposite: you are able
to use lowercase, but uppercase produces errors. Conclusion: try
both in troubles!

Note also that a typical mistake is to forget one of the right parenthesis or
quotes ({\bf `` }). So maybe a couple of them might be useful to leave
Lisp safely.

During the session you can get some kind of messages from Lisp.
All of them start with stars. The message that starts from five stars
$*****$ means an error. Three stars $***$ mean a minor error or
only a warning --
it is possible to continue the work.
\begin{exm} Here we forget to write the parenthesis.
\end{exm}

{\bf
2 lisp$>$ simple

***** `simple' is an unbound ID

3 lisp$>$
}





\section{Simplest calculations in {\bf  bergman}}

 Here we describe and explain
 several examples that you can easily copy and modify.


The simplest way to employ {\bf bergman} is to start it, to use some
specially written routines
such as
\index{simple} {\bf simple} or \index{ncpbhgroebner} {\bf
  ncpbhgroebner,}  to
feed it input interactively or by means of an input file prepared in
advance, and to quit.

In a slightly more sophisticated use, you may
influence the behavior by various "mode changing" procedures and
flags.

In very sophisticated use, you may employ and expand the
experimental procedures enclosed to the programme, and/or interact
directly with the underlying lisp representations of the algebraic
objects.

You also have access to all source code and can use all procedures
to implement your own applications.

This chapter covers the first use.  For more
sophisticated use guidance see the next chapters.

\subsection{Selecting alternatives}

{\bf Bergman} works in different modes, a full overview you can find
in \ref{refm}.

To perform some computations in {\bf bergman} it is necessary at least
to set up the polynomial ring selecting commutative or non-commutative
alternative,  \index{ordering} ordering \index{revlexify} ({\bf revlexify} or
\index{deglexify}  {\bf deglexify}),
\index{coefficients} coefficients field
(characteristic \index{characteristic}  0, 2 or arbitrary primary),
\index{weights} weights of variables etc.

For the first examples we  skip the complete description of alternatives
using the corresponding setting included in the main top level procedures.
We shall distinguish here only commutative and non-commutative calculations.

\subsection{Commutative algebras}



Let us start with an example of \index{Gr\"{o}bner basis} Gr\"{o}bner
basis computation for an ideal.
It will be performed by the procedure \index{simple} {\bf simple}. There are several
way to call this procedure explained completely in \ref{procsimple}.
Here we  illustrate two of them.

Calling {\bf simple} without arguments you can introduce the relations
directly from the screen following the prompt and respecting one
restriction: the relations must be homogeneous. Here is an example of
the session:                

\begin{verbatim}
1 lisp> (SIMPLE)
Now input in-variables and ideal generators in algebraic form, thus:
      vars v1, ..., vn;
      r1, ..., rm;
where v1, ..., vn are the variables, and r1, ..., rm the generators.
algebraic form (comm.) input> vars x,y;x^2-y^2,x*y;
% 2
x*y,
   x^2-y^2,

% 3
y^3,

Done
 - All is OK (I hope). Now you may (e. g.):
   - kill bergman with (QUIT); or
   - interrupt bergman with ^Z; or
   - clear the memory with (CLEARALL), and run a new (SIMPLE).
nil
2 lisp>
\end{verbatim}

The result contains three elements of Gr\"{o}bner basis : $x*y, x^2-y^2, y^3.$
Note that, according to the order of variables, x (the first in the list)
is highest, so xx, xy and yyy are highest monomials (obstructions).
Thus, only the monomials 1, x, y, yy are normal (do not divisible by the
obstructions) and serve as a basis of our algebra. Its
dimension is equal 4  and we have here a multiplication table too.
(All elements of the Gr\"{o}bner basis are equal to zero in the
quotient algebra, so we can
use their highest terms for the reduction of non-normal words).
For example, x$*$x=yy, y$*$x=yx, y$*$yy=0, x$*$yy=0.

As was said above, \index{simple} {\bf simple} may be called in several ways. One
of them is to  perform input and output by means of  files.
Let us prepare the following one (suppose that its name is "test1.a".
Check its existence in the directory {\bf bergman/tests} and copy it
into your current directory):

{\bf
\index{algforminput} (algforminput)

vars x,y;

x$*$x$-$y$*$y, x$*$y;
}

The first line informs {\bf bergman} that the succeeding lines are
input data in the \index{algebraic form} algebraic form. It means that you need to write
multiplication symbol $*$ or powers for example $x\wedge 2$ or x$**2$ instead of
 x$*$x but not $xx$
 (the same as in Reduce or Maple). Other possibility is
 \index{Lisp form} Lisp--form;
 read about them in the subsection \ref{inputform}.

 The next two lines are input data themselves. The first one contains variables, they should be
 written between keyword \index{vars} {\bf vars} and semicolon. Then comes the
 defining relations,
 separated by commas and finished by semicolon.

 To start the calculation select the name for output file, for example
 "test1.bg"
 (it should not
 exists!), start {\bf bergman}, switch to the Lisp
 mode and write

 {\bf (simple "test1.a" "test1.bg")}

 (do not forget double quotes!) and then quit.

The following is the full session of our work.

\begin{verbatim}

bergman

4: end;
Entering LISP ...
Bergman 0.922,  9-Jun-98
1 lisp> (SIMPLE "test1.a" "test.bg")
nil
 - All is OK (I hope). Now you may (e. g.):
   - kill bergman with (QUIT); or
   - interrupt bergman with ^Z; or
   - clear the memory with (CLEARALL), and run a new (SIMPLE).
nil
2 lisp> (quit)

Quitting
\end{verbatim}

Here is the resulting file "test1.bg", containing Gr\"{o}bner basis
\begin{verbatim}
% 2
x*y,
   x^2-y^2,

% 3
y^3,

Done


\end{verbatim}



\subsection{Non-commutative algebras}

The procedure \index{simple} {\bf simple} may be used to perform
non-commutative \index{Gr\"{o}bner basis} Gr\"{o}bner basis computations also.
{\bf Bergman} by default is in commutative mode, so,
first of all we need to turn it to non-commutative calculations.

 Here is an example of the session:

\begin{verbatim}
2 lisp> (NONCOMMIFY)
nil
3 lisp>  (SIMPLE)
Now input in-variables and ideal generators in algebraic form, thus:
      vars v1, ..., vn;
      r1, ..., rm;
where v1, ..., vn are the variables, and r1, ..., rm the generators.
algebraic form (noncomm.) input> vars x,y;x^2-y^2,x*y;
+t^2*(2*z^2)
% 2
x*y,
   -y*y+x*x,

+t^3*(2*z^2+2*z^3)
% 3
x*x*x,
   y*x*x,

+t^4*(6*z^3+2*z^4)
Done
 - All is OK (I hope). Now you may (e. g.):
   - kill bergman with (QUIT); or
   - interrupt bergman with ^Z; or
   - clear the memory with (CLEARALL), and run a new (SIMPLE).
nil

\end{verbatim}



Note that the session itself looks slightly different from the commutative
case: four terms of the monomial \index{Poincar\'{e} series}
Poincar\'{e} series were
printed. And, of course, we have different output of \index{Gr\"{o}bner basis}
Gr\"{o}bner basis too (although variables and generators looks the same as in the commutative case).

According to the order of variables, y (the last in the list --
opposite to the defaults in the commutative case)
is the highest, so xy, yy, xxx and yxx are the highest monomials (obstructions).
Thus only monomials 1, x, y, xx, yx are normal (do not contain
obstructions as subwords) and serve as a basis of our algebra. Its
dimension is equal 5 and we have here a multiplication table too.
(All elements of the Gr\"{o}bner basis are equal to zero in algebra, thus
we can
use their highest terms for the reduction of non-normal words).
For example, x$*$x=xx, y$*$y=xx, x$*$xx=0.

In the non-commutative case the homogeneity restriction must be
respected also.

The input can be performed also by means of a file.
Let us prepare the following one (suppose that its name is "test2.a".
Check its existence in the directory {\bf bergman/tests} and copy it
into your current directory):

\index{noncommify} {\bf (noncommify)

(setq maxdegree 10)}

\index{algforminput} {\bf (algforminput)

vars x,y;

x$*$x$-$y$*$y,x$*$y;
}

The first line switches {\bf bergman} to the non-commutative mode.
The second line was not necessary in this example. It restricts
calculations up to degree 10. Here calculations stops in degree 3
(as you will see later), but in general \index{Gr\"{o}bner basis}
Gr\"{o}bner basis might be
infinite so it is recommended to restrict the degree of calculations
(although {\bf bergman} will try to do them until it will be insufficient
memory).

The third line informs {\bf bergman} that the following is  input data in the
 \index{algebraic form} algebraic form. It means that you need to write multiplication
 symbol $*$ or powers for example $x\wedge 2$ or
 $x**2$ instead of $ x*x$ but not $xx$
 (the same as in Reduce or Maple). Other possibility is
\index{Lisp form} Lisp--form; read about them in the section \ref{inputform}.

 Next two lines are input data themselves. The first contains variables, they should be
 written between keyword \index{vars} {\bf vars} and semicolon. Than the
 generators,
 separated by commas and finished by semicolon.

 To start the calculation select the name for output file,
 for example "test2.bg" (it should not
 exist!), for example "test2.bg", start {\bf bergman}, switch to the Lisp
 mode and write\\
{\bf (simple "test2.a" "test2.bg")}\\
(do not forget double quotes!) and then quit.

The following is the full session of our work.

\begin{verbatim}

> bergman

4: end;
Entering LISP ...
Bergman 0.922,  9-Jun-98
1 lisp> (SIMPLE "test2.a" "test2.bg")
nil
10
nil
+t^2*(2*z^2)
+t^3*(2*z^2+2*z^3)
+t^4*(6*z^3+2*z^4)
 - All is OK (I hope). Now you may (e. g.):
   - kill bergman with (QUIT); or
   - interrupt bergman with ^Z; or
   - clear the memory with (CLEARALL), and run a new (SIMPLE).
nil
2 lisp> (quit)

Quitting
\end{verbatim}


The file {\bf test2.bg} contains the corresponding \index{Gr\"{o}bner basis}
Gr\"{o}bner basis:

\begin{verbatim}

% 2
x*y,
   -y*y+x*x,

% 3
x*x*x,
   y*x*x,

Done

\end{verbatim}


In the next example we use another procedure, working with \\
non-commutative algebras only and calculating besides the Gr\"{o}bner basis
of the algebra the Hilbert and monomial \index{Poincar\'{e} series}
Poincar\'{e} series.
There is no screen input, we should use files only.
We can use the same {\bf test2.a}
for input and {\bf test2.bg } for output (supposing that the output file
does not exist. If there is such file in your current directory remove
or rename it) and want to get two new files: ''test2.hs'' for
the
\index{Hilbert series} Hilbert series and ''test2.pbs'' for the Poincar\'e series. The procedure has
a name \index{ncpbhgroebner} {\bf ncpbhgroebner} (from NonCommutative Poincar\'e--Betti and
Hilbert series)
and it always
has 4 parameters. Here is a session (messages can be different):

\begin{verbatim}
1 lisp> (NCPBHGROEBNER "test2.a" "test2.bg" "test2.pbs" "test2.hs")
*** I turn on noncommutativity
nil
10
nil
*** Function `degreeenddisplay' has been redefined

% No. of Spolynomials calculated until degree 2: 0
% No. of ReducePol(0) demanded until degree 2: 0
% Time: 425

% No. of Spolynomials calculated until degree 3: 2
% No. of ReducePol(0) demanded until degree 3: 0
% Time: 646

% No. of Spolynomials calculated until degree 4: 8
% No. of ReducePol(0) demanded until degree 4: 5
% Time: 833
*** Function `degreeenddisplay' has been redefined
nil
2 lisp>
\end{verbatim}

File "test2.hs" for \index{Hilbert series} Hilbert series looks now as:

\begin{verbatim}
+2*z^2
+0*z^3
+0*z^4
\end{verbatim}

(note that known from the very beginning part \verb- 1+2*z- is absent here)
but file "test2.pbs" for the monomial \index{Poincar\'{e} series}
Poincar\'{e} series looks as:



\begin{verbatim}
+t^2*(2*z^2)
+t^3*(2*z^2+2*z^3)
+t^4*(6*z^3+2*z^4)
\end{verbatim}

and also does not contain first terms \verb- 1+t*(2*z).-

Note also that neither series contains terms in degree more
than 4 -- the last degree where {\bf bergman} have done some calculations.
Look to the next section if you need more terms.

But you will be especially surprised investigating  "test2.bg."
It looks now as:

\begin{verbatim}
% 2
((1 1 2))
((-1 2 2) (1 1 1))

% 3
((1 1 1 1))
((1 2 1 1))

Done
\end{verbatim}

It means that the result was written in the \index{Lisp form} Lisp--form. You can try to
understand it (it is the same as before -- try to recognize it!) or
read more
about this in the subsection \ref{inputform}, but let us
now change the input file a little and name it "test3.bg."

\begin{verbatim}
(noncommify)
(setq maxdegree 10)
(setq algoutmode 'ALG)
(algforminput)
vars x,y;
x*x-y*y,x*y;
\end{verbatim}

The session now looks almost the same (but the message about \index{algebraic
form} algebraic form appeared):

\begin{verbatim}

1 lisp> (NCPBHGROEBNER "test3.a" "test3.bg" "test3.pbs" "test3.hs")
*** I turn on noncommutativity
nil
10
alg
nil
*** Function `degreeenddisplay' has been redefined

% No. of Spolynomials calculated until degree 2: 0
% No. of ReducePol(0) demanded until degree 2: 0
% Time: 442

% No. of Spolynomials calculated until degree 3: 2
% No. of ReducePol(0) demanded until degree 3: 0
% Time: 629

% No. of Spolynomials calculated until degree 4: 8
% No. of ReducePol(0) demanded until degree 4: 5
% Time: 833
*** Function `degreeenddisplay' has been redefined
nil
2 lisp>

\end{verbatim}

But the file "test3.bg" now will be the same as "test2.bg" in the
example with {\bf simple }:

\begin{verbatim}

% 2
x*y,
   -y*y+x*x,

% 3
x*x*x,
   y*x*x,

Done
\end{verbatim}

\subsection {Starting a new calculation: the procedure {\bf clearall}}


We hope your first experiments with {\bf bergman} were successful and
following the prompt after calculations you can:

\begin{itemize}
\item  kill {\bf  bergman} with (QUIT); or
\item  interrupt {\bf  bergman}  with $\wedge Z $; or
\item  clear the memory with (CLEARALL), and run a new (SIMPLE).
\end{itemize}

Presuming you would like to run a new computation 
let us explain more carefully what  the function  \index{clearall}
{\bf clearall} is doing.

Despite of this name it does not clear all that was done before, but
only clear memory from the results of the previous calculations.

You always {\it should} call this function before starting a new cycle of
the calculations. The only exclusion is when you want to add some
new elements to the already calculated Gr\"{o}bner basis , but for doing this kind of
staff you should be a professional. So, once again, in this chapter:
{\em before the calling second time one of the top--of--the--top procedures, such
as} {\bf simple, ncpbhgroebner} {\em always call} {\bf clearall}

You need not do it from the very beginning, but you need to know
what it really clears. {\em It clears:}
 \begin{itemize}
 \item initial generators,
 \item calculated Gr\"{o}bner basis,
 \item all the memory, used for the calculations.
 \end{itemize}

{\em  It saves:}
  \begin{itemize}
 \item all selected modes (see section \ref{modes}), including
    list of input variables.
  \end{itemize}

You can use this possibility: {\em do not introduce the same set of variables,
skipping } {\bf vars \ldots}

 \begin{exm}  Several computations with the same list of input
   variables.
\begin{verbatim}   
1 lisp> (SIMPLE)
Now input in-variables and ideal generators in algebraic form, thus:
        vars v1, ..., vn;
        r1, ..., rm;
where v1, ..., vn are the variables, and r1, ..., rm the generators.
algebraic form (comm.) input> vars x,y,z;
algebraic form (comm.) input> x^3, x^2*y-y^2*x, z^2*x, x^2*z-y^2*z;
% 3
x*z^2,
   x^2*z-y^2*z,
   x^2*y-x*y^2,
   x^3,
   
% 4
y^2*z^2,
   y^3*z,
   x*y^2*z,
   x*y^3,
   
Done
 - All is OK (I hope). Now you may (e. g.):
   - kill bergman with (QUIT); or
   - interrupt bergman with ^Z; or
   - clear the memory with (CLEARALL), and run a new (SIMPLE).
nil
2 lisp> (CLEARALL)
nil
3 lisp> (SIMPLE)
Now input in-variables and ideal generators in algebraic form, thus:
        vars v1, ..., vn;
        r1, ..., rm;
where v1, ..., vn are the variables, and r1, ..., rm the generators.
algebraic form (comm.) input> y*z^2-z^3, x^2*z-y*z^2, x*y*z;
% 3
y*z^2-z^3,
   x*y*z,
   x^2*z-z^3,
   
% 4
z^4,
   x*z^3,
   
Done
 - All is OK (I hope). Now you may (e. g.):
   - kill bergman with (QUIT); or
   - interrupt bergman with ^Z; or
   - clear the memory with (CLEARALL), and run a new (SIMPLE).
nil
4 lisp>

\end{verbatim}
 \end{exm}











