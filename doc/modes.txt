
	% THE MODE DESIGNATOR TREE STRUCTURE.

 Last update: 2006-08-20 (JoeB)

 This is intended to be an overview of the names used
for modes and submodes in the compact mode handler.  It
should be updated, whenever the names or organisation
of this changes.  In principal, each line will contain
a mode designator, and may contain a list of valid
values of this.  However, if the latter list is rather
long, it may continue over several lines.

 Sub-modes are denoted by indentation; and top level
modes are separated by empty lines.  Thus

 XXXXX <...>
    YYYYY
    ZZZZZ <...>

 WWWWW <...>

would denote two major modes XXXXX and WWWWW (two nodes
immediately under the mode tree root), while YYYYY
and ZZZZZ would be minor modes (nodes immediately
under XXXXX).  In addition, 'legal values' for XXXXX,
ZZZZZ, and WWWWW would be enumerated, starting with
the default values.

 Note: This is mainly an extract from the existing code.
There may be errors in details.  Please inform me of such
ones!/JoeB

 Noted inconsistencies or omissions are marked by an asterisk.


 OBJECTTYPE  <RING | MODULE | TWOMODULES | FACTORALGEBRA>
    AUTOADDRELATIONSTATE  <NIL NIL | T NIL | NIL T>
    LOWTERMSHANDLING	  <QUICK | SAFE>

 RESOLUTIONTYPE  <NONE | ANICK | MINIMAL>

 ANICK
    TENSPOL-PRINTMODE	  <SEMIDISTRIBUTED | DISTRIBUTED>
 *  TENSTERM-PRINTMODE    <NIL | <a string>> <NIL | <a string>> <NIL | <a string>> ??
    EDGE-STRING           <<a string>>
    EDGE-STRING-PRINTING  <NIL | T>
    BETTI		  <NIL | T>
    BETTI-FOREACHDEGREE   <NIL | T>
 *  <the Anick output file>

 VARIABLESETUP
    RINGTYPE		  <COMMUTATIVE | NONCOMMUTATIVE>
    COMMUTATIVEORDER	  <TDEGLEX | PURELEX | TDEGREVLEX | MATRIX>
    ORDERMATRIX		   <<a list of lists of integers>>
    NONCOMMUTATIVEORDER   <TDEGLEFTLEX | ELIMLEFTLEX
			   | HOMOGELIMLEFTLEX | INVELIMLEFTLEX>
    VARIABLENUMBER	  <NIL | <a non-negative integer>>
    INVARIABLES		  <<a list of different identifiers>>
    OUTVARIABLES	  <<a list of different identifiers>>
 *  VARIABLEWEIGHTS
    HOMOGENISATIONVARIABLE <<an identifier>>

 STRATEGY		  <ORDINARY | RABBIT | SAWS>
    PROCESS		  <DEGREEWISE | ITEMWISE>
    STABILITY		  <T | NIL>
 *  AUTOREDUCTION	  <IMMEDIATE | POSTPONED>
 *  CONTENTS		  <DENSE | SPARSE>
    MINDEG		  <NIL | <a non-negative integer>>
    MAXDEG		  <NIL | <a non-negative integer>>
    INTERRUPTION	  <ORDINARY | MINHILBLIMITS>
 *  HSERIESLIMITATIONS
    CUSTOMISED		  <NIL | T>

 COEFFICIENTDOMAIN	  <NIL | SF | 0 | <a prime>>
    ODDPRIMESMODULI	  <ORDINARY | MODLOGARITHMIC>

 INPUTFORMAT		  <CASESENSALGIN | NOCASESENSALGIN>

 OUTPUTFORMAT		  <LISP | ALG | MACAULAY> <NOEXPTSPRINT | EXPTSPRINT>
    PRINTTIMES
	DEGREEPRINTOUTPUT <NIL | T>
	ITEMPRINTOUTPUT	  <NIL | T | MAYBE>
 *	FINALPRINTOUTPUT  <MAYBE | T | NIL>
    IMMEDIATEASSOCRINGPBDISPLAY   <NIL | T>
    OVERWRITE		  <NIL | T>
    CUSTOMISED		  <NIL | T>

 DEBUGSTATUS
    DIRECTALERTCLOSING	  <NIL | T>
    CHECKFAILUREDISPLAY	  <T | NIL>
    ACTIVETRACEBREAKS	  <NIL | T>
 VARIOUSFLAGS
    DYNAMICLOGTABLES	  <NIL | T>	* Subsubmode to COEFFICIENTDOMAIN?
 *  (HOMOGENISED)	  [Removed.]
    IMMEDIATECRIT	  <NIL | T>
 *  IMMEDIATEFULLREDUCTION <T | NIL>
    IMMEDIATERECDEFINE	  <NIL | T>
    NOBIGNUM		  <NIL | T>
    OBSOLETENAMESINFORM	  <NIL | T>
    OLDPRINTMODES1	  <NIL | T>
    OLDSERIESMODES1	  <NIL | T>
    STANDARDANICKMODES	  <T | NIL>	* Submode to ANICK?
    PBSERIES		  <NIL | T>
    REDUCTIVITY		  <T | NIL>	* Submode to STRATEGY?
    SAVEDEGREE		  <NIL | T>	* Submode to OUTPUTFORMAT?
    SAVERECVALUES	  <T | NIL>	* Submode to STRATEGY?


Comments: The following legal values are not defined by
the CARs of alists; but they should be (enabling QuizIds
dialogues):

 OBJECTTYPE, ODDPRIMESMODULI, INPUTFORMAT, OUPUTFORMAT.

((The following legal values are given by alists, but the
  QuizIds feature is not used: None found right now.))

The following might be added into the compact mode handling:
 MAXSERDEG,
 AQDDRELATIONS,
 HOMOGENITY,
 SAWS, RABBIT,

The VARIOUSFLAGS list should be subject to a lot of changes.
We should be careful not to make it too accessible.
They actually correspond to switches (so that to the
VARIOUSFLAGS item FOO there corresponds a switch *FOO, with
legal values NIL and T). Used in a modesetting, their
permitted values are  <NIL | T>.

Presently (2006-08-20), the following variousflags may be
accessed by specific SET/GET procedures:

 DYNAMICLOGTABLES, IMMEDIATECRIT, IMMEDIATEFULLREDUCTION,
 IMMEDIATERECDEFINE, NOBIGNUM, OBSOLETENAMESINFORM, PBSERIES[*],
 REDUCTIVITY, SAVEDEGREE, SAVERECVALUES

(but note that the mode changers for PBSERIES are called
 (SET/GET)PBSERMODE).

Lacking such are

 OLDPRINTMODES1, OLDSERIESMODES1, STANDARDANICKMODES

HOMOGENISED should not be directly accessible by the user; and
should not be documented now.

The two "obsolete oldies" might be discarded for the time being.

STANDARDANICKMODES is a relatively new thing. I'll have to think it
over a bit. Could be left out now, anyhow.

Thus, I recommend documenting the ten switches with mode
changers, not the others.
