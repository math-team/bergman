\contentsline {chapter}{\numberline {1}A brief bergman tutorial}{7}
\contentsline {section}{\numberline {1.1}Preliminaries}{8}
\contentsline {subsection}{\numberline {1.1.1}Starting a {\bf bergman} session}{9}
\contentsline {subsection}{\numberline {1.1.2}Ending a {\bf bergman} session}{10}
\contentsline {subsection}{\numberline {1.1.3}What you need to know about Lisp syntax}{10}
\contentsline {section}{\numberline {1.2}Simplest calculations in {\bf bergman}}{11}
\contentsline {subsection}{\numberline {1.2.1}Selecting alternatives}{12}
\contentsline {subsection}{\numberline {1.2.2}Commutative algebras}{12}
\contentsline {subsection}{\numberline {1.2.3}Non-commutative algebras}{14}
\contentsline {subsection}{\numberline {1.2.4}Normal form and reduction}{18}
\contentsline {subsection}{\numberline {1.2.5}Starting a new calculation: the procedure {\bf clearideal}}{20}
\contentsline {chapter}{\numberline {2}Computations in bergman}{25}
\contentsline {section}{\numberline {2.1}Introduction}{26}
\contentsline {section}{\numberline {2.2}{\bf Bergman}: philosophy and approach}{26}
\contentsline {section}{\numberline {2.3}{\bf Bergman}: main restrictions}{26}
\contentsline {section}{\numberline {2.4}The polynomial ring set up}{27}
\contentsline {subsection}{\numberline {2.4.1}Background}{27}
\contentsline {subsection}{\numberline {2.4.2}Variable names and the flag {\bf raise} }{27}
\contentsline {subsection}{\numberline {2.4.3}Ordering}{29}
\contentsline {subsection}{\numberline {2.4.4}Using eliminating ordering}{30}
\contentsline {subsection}{\numberline {2.4.5}Coefficients: background}{32}
\contentsline {subsection}{\numberline {2.4.6}Coefficients: Choices}{33}
\contentsline {subsection}{\numberline {2.4.7}Weights handling}{35}
\contentsline {subsection}{\numberline {2.4.8}Homogenisation}{37}
\contentsline {subsection}{\numberline {2.4.9}Strategies}{39}
\contentsline {section}{\numberline {2.5}Ideals and quotient ring}{39}
\contentsline {subsection}{\numberline {2.5.1}Input--output modes for the polynomials}{40}
\contentsline {section}{\numberline {2.6}Calculating and using series}{42}
\contentsline {subsection}{\numberline {2.6.1}Hilbert series computation}{42}
\contentsline {subsection}{\numberline {2.6.2}Computation of Gr\"{o}bner basis using known Hilbert series}{49}
\contentsline {section}{\numberline {2.7}The jumping rabbit}{51}
\contentsline {section}{\numberline {2.8}Some bricks to build your own procedures}{53}
\contentsline {section}{\numberline {2.9}Modules over non-commutative algebras}{55}
\contentsline {subsection}{\numberline {2.9.1} Gr\"{o}bner basis for modules over non-commutative algebras}{55}
\contentsline {subsection}{\numberline {2.9.2}Gr\"{o}bner basis for modules over non-commutative algebras}{56}
\contentsline {subsection}{\numberline {2.9.3}Hilbert series for modules over non-commutative algebras}{58}
\contentsline {subsection}{\numberline {2.9.4}The Anick resolution and Betti numbers for the trivial module}{63}
\contentsline {subsection}{\numberline {2.9.5}Writing own procedures}{66}
\contentsline {subsection}{\numberline {2.9.6}The Anick resolution and Betti numbers for a right module}{68}
\contentsline {subsection}{\numberline {2.9.7}The Anick resolution and Betti numbers for right ideals, two-sided ideals, and factor-algebras, considered as right modules}{73}
\contentsline {subsection}{\numberline {2.9.8}Working with the left modules}{79}
\contentsline {subsection}{\numberline {2.9.9} Betti numbers for two modules}{85}
\contentsline {subsection}{\numberline {2.9.10}Calculating Hochschild homology of an algebra}{90}
\contentsline {section}{\numberline {2.10}Bergman under Reduce}{97}
\contentsline {subsection}{\numberline {2.10.1}Working in the Reduce syntax}{97}
\contentsline {subsection}{\numberline {2.10.2}Parametric coefficients}{98}
\contentsline {section}{\numberline {2.11}Debugging in {\bf bergman}}{101}
\contentsline {chapter}{\numberline {3}Bergman for the advanced user}{109}
\contentsline {section}{\numberline {3.1}The top--of--the--top level procedures}{110}
\contentsline {subsection}{\numberline {3.1.1}Procedure {\bf simple}}{110}
\contentsline {subsection}{\numberline {3.1.2}Procedure {\bf ncpbhgroebner}}{110}
\contentsline {subsection}{\numberline {3.1.3}Procedure {\bf modulehseries}}{111}
\contentsline {section}{\numberline {3.2}References}{112}
\contentsline {subsection}{\numberline {3.2.1}Sustained operating mode alternatives}{112}
\contentsline {subsection}{\numberline {3.2.2}The mode changing procedures}{114}
\contentsline {subsection}{\numberline {3.2.3}The list of the top level procedures}{118}
\contentsline {subsection}{\numberline {3.2.4}User available flags and variables}{123}
\contentsline {subsection}{\numberline {3.2.5}User available counters}{128}
\contentsline {subsection}{\numberline {3.2.6}User readable variables}{129}
\contentsline {subsection}{\numberline {3.2.7}User defined variables}{129}
\contentsline {section}{\numberline {3.3}The commutative Hilbert series procedures}{130}
