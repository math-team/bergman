(SETQ TOPLOOPNAME!* "")

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Copyright (C) 1992,1994,1995,1996,2002,2005 Joergen Backelin
%%
%% Bergman is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY.  No author or distributor
%% accepts responsibility to anyone for the consequences of using it
%% or for whether it serves any particular purpose or works at all,
%% unless (s)he says so in writing.  Refer to the Bergman General
%% License Public for full details.

%% Everyone is granted permission to copy, modify and redistribute
%% bergman, but only under the conditions described in the
%% Bergman General Public License.   A copy of this license is
%% supposed to have been given to you along with bergman so you
%% can know your rights and responsibilities.  It should be in a
%% file named copyright.  Among other things, the copyright notice
%% and this notice must be preserved on all copies.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%	CHANGES:

%  Moved first instructions to new header./JoeB 2005-06-30 -- 07-01

% dialogue added./JoeB 2004-04-17

% hscomm added./JoeB 2002-10-12

%  pbseries removed./JoeB 1999-11-04

%  modinout.sl, modes.sl added/JoeB 1997-09-17

%  The architecture handling adopted to `kzz type'/JoeB 1996-11-26.

%14.08.96 Svetlana Cojocaru, Victor Ufnarovski
%The module "full" is divided into two parts to respect the restriction
%on the length of a FASLOUT output file - no more than 64K.
%The module sermul.sl is included additionaly (series multiplication).


(LAPIN (MKBMPATHEXPAND "$bmsrc/macros.sl"))
(OFF RAISE)
(RECLAIM)

% modes.sl contains some macro definitions used later; thus
% compile it first!

(FASLOUT (MKBMPATHEXPAND "$bmload/full"))
% (OFF USERMODE)
(DSKIN (MKBMPATHEXPAND "$bmsrc/modes.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/main.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/inout.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/monom.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/ncmonom.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/normwd.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/reclaim.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/polynom.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/reduct.sl"))
(FASLEND)
(RECLAIM)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%14.08.96 Svetlana Cojocaru, Victor Ufnarovski
%The module "full" is divided into two parts to respect the restriction
%on the length of a FASLOUT output file - no more than 64K.
%The module sermul.sl is included additionaly (series multiplication).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

(FASLOUT (MKBMPATHEXPAND "$bmload/full1"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/coeff.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/char0.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/strategy.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/dialogue.sl"))
(FASLEND)
(RECLAIM)

% Compilation of auxiliary units:

% Removed 1999-11-04, since pbseries now is subsumed in hseries./JoeB
%(FASLOUT (MKBMPATHEXPAND "$bmload/pbseries"))
%(DSKIN (MKBMPATHEXPAND "$bmsrc/pbseries.sl"))
%(FASLEND)
%(RECLAIM)

(COND ((FILEP (MKBMPATHEXPAND "$bmexe/alg2lsp.sl"))
       (FASLOUT (MKBMPATHEXPAND "$bmload/alg2lsp"))
       (DSKIN (MKBMPATHEXPAND "$bmexe/alg2lsp.sl"))
       (FASLEND)
       (RECLAIM))
      (T
       (PRIN2 "*** I could not find alg2lsp.sl; hope it's OK")
       (TERPRI)))

(FASLOUT (MKBMPATHEXPAND "$bmload/char2"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/char2.sl"))
(FASLEND)
(RECLAIM)

% Extras (under development):
(FASLOUT (MKBMPATHEXPAND "$bmload/auxil"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/auxil.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/homog.sl"))
(FASLEND)
(RECLAIM)

% Staggering substantial stuff:
% (OFF REDEFMSG) (OFF USERMODE) (ON GC)
(LAPIN (MKBMPATHEXPAND "$bmsrc/stg/accmacr.sl"))
(LAPIN (MKBMPATHEXPAND "$bmsrc/stg/subsmacr.sl"))
(LAPIN (MKBMPATHEXPAND "$bmsrc/stg/midmacr.sl"))
(OFF RAISE)
% (ON REDEFMSG)
(FASLOUT (MKBMPATHEXPAND "$bmload/stg"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/stg/checkstg.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/stg/accproc.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/stg/subsproc.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/stg/stg.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/stg/monomstg.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/stg/bind.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/stg/write.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/stg/ideal.sl"))
(FASLEND)
(RECLAIM)

%  New July and August 1996:
(LAPIN (MKBMPATHEXPAND "$bmsrc/hmacro.sl"))
(FASLOUT (MKBMPATHEXPAND "$bmload/hseries"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/hseries.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/hscomm.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/modinout.sl"))
(DSKIN (MKBMPATHEXPAND "$bmsrc/sermul.sl"))
(FASLEND)
(RECLAIM)

% Set-up dependent compilation demands, et cetera.

(COND ((FILEP (MKBMPATHEXPAND "$bmexe/speccmp2.sl"))
       (LAPIN (MKBMPATHEXPAND "$bmexe/speccmp2.sl"))))

(QUIT)
